﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ILogger = NLog.ILogger;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CatController : ControllerBase
    {
        private static readonly List<Cat> _cats = new List<Cat>
        {
            new Cat {Name = "Mimì"},
            new Cat {Name = "Ermellino"},
            new Cat {Name = "Garfield"},
            new Cat {Name = "Eden"},
        };

        private readonly ILogger<CatController> _logger;

        public CatController(ILogger<CatController> logger)
        {
            _logger = logger;
        }

        [HttpGet] // https://localhost:5001/Cat
        public ActionResult<IEnumerable<Cat>> Get()
        {
            try
            {
                _logger.LogInformation(@"Action /Cat started");

                throw new Exception("My custom exception");

                return Ok(_cats);
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return StatusCode(500, e);
            }
        }

        [HttpGet("{name}")] // https://localhost:5001/Cat/<name>
        public ActionResult<Cat> Get(string name)
        {
            try
            {
                var cat = _cats.FirstOrDefault(c => c.Name.ToLower() == name.ToLower());
                return Ok(cat);
            }
            catch (Exception e)
            {
                //Loggo l'eccezione e poi..
                return StatusCode(500);
            }
        }

        [HttpPost]
        public ActionResult Post(Cat cat)
        {
            try
            {
                _cats.Add(cat);

                return StatusCode(201);
            }
            catch (Exception e)
            {
                //Loggo l'eccezione e poi..
                return StatusCode(500);
            }
        }
    }

    public class Cat
    {
        public string Name { get; set; }
        public string Sentence { get; set; } = "Meow";
    }
}
