﻿using System;
using System.Runtime.CompilerServices;

namespace ClassesP2
{
    public class Car // In C#, le properties sono un modo rapido per fare incapsulamento (controllare l'accesso a un campo private)
    {
        //private string _brand; // Backing field

        //public string Brand  // Full property
        //{
        //    get
        //    {
        //        var editedBrand = $"Brand is: {_brand}";
        //        return editedBrand;
        //    }

        //    set
        //    {
        //        if(value == null)
        //            throw new Exception("Brand cannot be null");
        //        _brand = value;
        //    }
        //}

        public string Brand { get; set; } // Auto property

        //public string GetBrand()
        //{
        //    return _brand;
        //}

        //public void SetBrand(string value)
        //{
        //    _brand = value;
        //}
    }

    /* CLASS, FIELD, METHODS MODIFIERS */

    abstract class Bicycle // Una classe abstract non può essere istanziata
    {
        public abstract string Name { get; set; }

        public abstract void Accelerate(); // Un metodo abstract non prevede implementazione

        public void GoStraight()
        {
            Console.WriteLine("Bycicle is going straight");
        }
    }

    class Motorcycle : Bicycle
    {
        public override string Name { get; set; }

        public sealed override void Accelerate() // Un metodo sealed non può essere ulteriormente
                                                 // soggetto a override
        {
            Console.WriteLine("Motorcycle is accelerating");
        }
    }

    class Chopper : Motorcycle
    {
    }

    sealed class RaceCar : Car // Una classe sealed non può essere ereditata
    {
        public static int CubicCms;

        public RaceCar()
        {
            
        }
        static RaceCar()
        {
            CubicCms = 1200;
        }

        public const int WheelsNumber = 4; // Impedisce che una variabile possa essere riassegnata.

        //public const int[] arr = new[] {1, 2, 3, 4};

        public readonly string Color; // La variabile può essere assegnata solo nel costruttore.
                                        // Da lì in poi diventa readonly 

        public RaceCar(string color)
        {
            Color = color;
        }
    }
        

    public static class CarsStatistics // la classe non può essere istanziata e può essere utilizzata
                                       // senza un'istanza diretta
    {
        static CarsStatistics() // Il costruttore statico serve a inizializzare i campi statici
                        // prima di tutto il resto, dunque anche prima dei campi non statici
        {

        }

        public static int TotalCars; // i campi e metodi di una classe statica
                                     // devono essere dichiarati statici

        public static int GetTotalCars()
        {
            return TotalCars;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var car1 = new Car();

            /* --- Get e Set senza usare le properties --- */

            //car1.SetBrand("Audi");
            //Console.WriteLine(car1.GetBrand());

            /* --- Get e Set usando le properties --- */

            car1.Brand = "Alfa";
            Console.WriteLine(car1.Brand);

            car1.Brand = null;

            Bicycle bicycle = new Motorcycle(); // Liskov substitution principle

            var raceCar1 = new RaceCar();
            Console.WriteLine(RaceCar.CubicCms);
            RaceCar.CubicCms = 1300;
            var raceCar2 = new RaceCar();
            Console.WriteLine(RaceCar.CubicCms);
        }
    }
}
