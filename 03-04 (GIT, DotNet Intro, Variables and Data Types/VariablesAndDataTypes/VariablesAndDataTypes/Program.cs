﻿using System;

namespace VariablesAndDataTypes
{ // per parentesi graffe: shift destro + altgr + è
    class Program
    {
        static void Main(string[] args)
        {
            /* DICHIARAZIONE VARIABILI */

            int number = 3; // inizializzazione

            string firstName = "Ugo"; // Per convenzione le variabili locali si scrivono in camelCase

            firstName = "Ago"; // Assegnazione

            string lastName; // Dichiarazione

            // age = 3; // Errore! Non ho mai dichiarato questa variabile

            int number_1 = 1; // I nomi delle variabili possono contenere lettere, numeri e underscore

            // int 1number = 1; // Ma devono sempre iniziare o con una lettera o con un underscore

            int count = 0;
            int Count = 0; // Il case conta

            // bool bool = false;  // Non ammesso, non puoi usare keyword riservate come identificatori

            bool @bool = true; // A meno che non ci sia l'operatore verbatim davanti

            // bool 1234 = false; // Non possono essere composte solo da numeri


            /* **** PRIMITIVE DATA TYPES **** */

            /* - Numeric data types - */

            // Byte / byte (1 byte)

            byte byteMin = 0;
            byte byteMax = 255;

            // Sbyte / sbyte (1 byte)

            sbyte sbyteMin = -128;
            sbyte sbyteMax = 127;

            // Int16 / short (2 byte)

            short shortMin = -32_768; // L'underscore è un digit separator
            short shortMax = 32767;

            // UInt16 / ushort (2 byte)

            ushort ushortMin = 0;
            ushort ushortMax = 65_535;

            // Int32 / int (4 byte)

            Int32 intMin = -2_147_483_648;
            int intMin2 = -2_147_483_648;

            int intMax = 2_147_483_647;
            int intMax2 = int.MaxValue;

            int hex = 0x0010; // Puoi scrivere esadecimali anteponendo 0x
            int bin = 0b0010; // puoi scrivere binari anteponendo 0b
            int oct = 014707; // Puoi scrivere ottali anteponendo 0

            // UInt32 / uint (4 byte)

            uint uintMin = 0;
            uint uintMax = 4_294_967_295;

            // Int64 / long (8 byte)

            long longMin = -9_223_372_036_854_775_808;
            long longMax = 9_223_372_036_854_775_807;

            // UInt64 / uint (8 byte)

            ulong ulongMin = 0;
            ulong ulongMax = 18_446_744_073_709_551_615;

            // Single / float (4 byte) - 7 significant digits

            float floatMin = -3.402823e38f;
            float floatMax = 3.402823e38F;

            // Double / double (8 byte) - 15 significant digits

            double doubleMin = -1.79769313486232e307;
            double doubleMax = 1.79769313486232e307;

            // Decimal / decimal (24 byte) - 28 to 29 significant digits

            decimal decimalMin = -79228162514264337593543950335M;
            decimal decimalMax = 79228162514264337593543950334M;

            /* ---Non numeric data types--- */

            // Char / char (2 byte) - un carattere UTF-16, che sta per Unicode Transformation Format 16 bit

            char letter = 'A'; // char tra apici singoli

            Console.WriteLine(char.MinValue);
            Console.WriteLine(char.MaxValue);

            char letterByNumber = (char) 65;

            Console.WriteLine("Il carattere UNICODE identificato da 65 è:" + letterByNumber);

            int numberByChar = 'A';

            Console.WriteLine("Il numero identificato da 'A' è:" + numberByChar);

            char chr1 = '\udfff';

            Console.WriteLine(chr1);

            // Boolean / bool (1 byte) - true o false

            bool isDotNetAmazing = true;
            Boolean falseValue = false;

            // String / string (no dimensione predefinita) - collezioni di caratteri racchiusi tra apici doppi

            string brand1 = "PlayStation";
            String brand2 = "Xbox";

            // Datetime (8 byte) - per rappresentare le date. E' un valuetype

            DateTime now = DateTime.Now;
            DateTime myDate = new DateTime(2021, 3, 4);

            Console.WriteLine("Now: " + now);

            Console.WriteLine($"MyDate: {myDate}");

            Console.WriteLine(DateTime.MinValue);
            Console.WriteLine(DateTime.MaxValue);

            Console.WriteLine(DateTime.UtcNow);

            Console.WriteLine("+1 giorno = " + myDate.AddDays(1));

            TimeSpan interval = DateTime.Now - new DateTime(1990, 9, 16);

            Console.WriteLine("Giorni dal 16/09/1990: " + interval.Days);
            //Console.WriteLine("Ore dal 16/09/1990: " + interval.Hours);
            Console.WriteLine("Ore dal 16/09/1990: " + interval.TotalHours);
            Console.WriteLine("Minuti dal 16/09/1990: " + interval.TotalMinutes);
            Console.WriteLine("Secondi dal 16/09/1990: " + interval.TotalSeconds);

            // Object / object (no dimensione predefinita) - classe da cui derivano tutti i tipi, valuetypes o reference types

            object objNum = 1;
            object objChar = 'c';
            object objString = "Hello";
            Object objBool = true;
            object objRandom = new Random();

            // Per ottenere il tipo di una variabile

            Console.WriteLine(objRandom.GetType());
            Console.WriteLine(objString.GetType());
        }
    }
}
