﻿using System;
using System.Threading;

// Gli using servono a comunicare alla CLR che volete far uso di un determinato namespace

namespace ClassesP1 // Namespace: raggruppamento logico di classi
{
    public class Author // Le classi 
    {
        public Author() // Questo è un costruttore (di default), un metodo particolare di una classe che serve a costruire un oggetto
        {

        }

        public Author(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public Author(string firstName, string lastName, string nation, DateTime dateOfBirth, string address, string city)
        {
            FirstName = firstName;
            LastName = lastName;
            Nation = nation;
            DateOfBirth = dateOfBirth;
            Address = address;
            _city = city;
        }

        public string FirstName;
        protected internal string LastName;
        internal string Nation;
        protected DateTime DateOfBirth;
        private protected string Address;
        private string _city; // Questo campo è private, ma posso controllarne l'accesso tramite dei metodi get e set

        public string GetCity()
        {
            var alteredCity = $"Città di: {_city}";
            return alteredCity;
        }

        public void SetCity(string city)
        {
            //if (city == null)
            //{
            //    _city = "Unknown";
            //}
            //else
            //{
            //    _city = city.Trim();
            //}
            _city = (city != null) ? city.Trim() : "Unknown";
        }

        public void PrintAuthor()
        {
            Console.WriteLine($"FirstName: {FirstName}, LastName: {LastName}, Nation: {Nation}, DateOfBirth: {DateOfBirth}, Address: {Address}, City: {_city}");
        }
    }

    public class Musician : Author
    {
        public Musician()
        {
            _writtenSongs = 0;
            _songs = new string[100];
        }

        public Musician(
            string firstName, 
            string lastName, 
            string nation, 
            DateTime dateOfBirth, 
            string address, 
            string city) : base(firstName, lastName, nation, dateOfBirth, address, city)
        {
            _writtenSongs = 0;
            _songs = new string[100];
        }

        private string[] _songs;
        private int _writtenSongs;

        public void RecordSong(string songTitle)
        {
            songTitle = "\"" + songTitle + "\"";
            _songs[_writtenSongs++] = songTitle;
            //++_writtenSongs;
            Console.WriteLine($"{FirstName} {LastName} has just recorded a new song named {songTitle}");
        }

        public void RecordSong(ref string songTitle) // Il parametro è passato per reference
        {
            songTitle = "\"" + songTitle + "\"";
            _songs[_writtenSongs++] = songTitle;
            //++_writtenSongs;
            Console.WriteLine($"{FirstName} {LastName} has just recorded a new song named {songTitle}");
        }

        public void RecordSongbyIn(in string songTitle) // Rende il parametro in ingresso read only
        {
            //songTitle = "\"" + songTitle + "\""; // Errore di compilazione
            _songs[_writtenSongs++] = songTitle;
            Console.WriteLine($"{FirstName} {LastName} has just recorded a new song named {songTitle}");
        }

        public void RecordSongByOut(out string song, string title) // Obbliga a passare il parametro senza valore assegnato
        {
            song = title;
            _songs[_writtenSongs++] = song;
            Console.WriteLine($"{FirstName} {LastName} has just recorded a new song named {song}");
        }
    }

    class Painter : Author
    {
        private string[] _subjects =
        {
            "a portrait",
            "a landscape",
            "a still-life",
            "an abstraction",
            "a nightmare",
            "a hole in a canvas"
        };

        public void PaintSomething()
        {
            int GetRandomNumber(int minValue, int maxValue)
            {
                var random = new Random();
                var innerRandomNumber = random.Next(minValue, maxValue);
                return innerRandomNumber;
            }

            var randomNumber = GetRandomNumber(0, _subjects.Length);

            Console.WriteLine($"{FirstName} {LastName} just painted {_subjects[randomNumber]}");
        }
    }

    class Program // Di default il modificatore di visibilità delle classi è internal
    {
        static void Main(string[] args)
        {
            Author author1 = new Author(); // Istanza di un oggetto a partire da una classe

            author1.FirstName = "Sergio";
            author1.LastName = "LoSpaccino";
            author1.SetCity(null);
            Console.WriteLine(author1.GetCity());

            author1.SetCity("Napoli");
            Console.WriteLine(author1.GetCity());

            var author2 = new Author(
                "Andrea",
                "Camilleri",
                "Italy",
                new DateTime(1925, 7, 6),
                "Unknown",
                "Porto Empedocle");

            author2.PrintAuthor();

            /* ---------------------------------- */

            var musician1 = new Musician();
            musician1.FirstName = "Dave";
            musician1.LastName = "Mustaine";

            string songName1 = "Holy wars";

            musician1.RecordSong(ref songName1);
            musician1.RecordSong(ref songName1);
            musician1.RecordSong(ref songName1);

            Console.WriteLine(songName1);

            /* ---------------------------------- */

            musician1.RecordSongByOut(out string song, "Lucretia");

            Console.WriteLine(song);

            /* ---------------------------------- */

            var painter1 = new Painter();

            painter1.FirstName = "Edward";
            painter1.LastName = "Hopper";

            //Thread.Sleep(1000);
            //painter1.PaintSomething();
            //Thread.Sleep(1000);
            //painter1.PaintSomething();
            //Thread.Sleep(1000);
            //painter1.PaintSomething();
            //Thread.Sleep(1000);
            //painter1.PaintSomething();
            //Thread.Sleep(1000);
            //painter1.PaintSomething();
            //Thread.Sleep(1000);
            //painter1.PaintSomething();
        }
    }
}
