﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using Microsoft.VisualBasic;

namespace LINQ // Linq è una libreria per l'interrogazione di collezioni.
    // Permettono di effettuare query sui dati in esse contenuti
    // Questa libreria si compone di una serie di extension methods richiamabili
    // Su oggetti di tipo IEnumerable
{
    /* Come si crea un extension method? */

    static class RandomExtensions
    {
        public static void Greet(this Random _, string name)
        {
            Console.WriteLine("Hello " + name);
        }

        public static void Greet(this Random _)
        {
            Console.WriteLine("Hello!");
        }
    }

    /* ******************************** */

    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte Age { get; set; }
        public int DepartmentId { get; set; }
    }

    public class Department
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class PhoneNumber
    {
        public string Number { get; set; }
    }

    public class Person
    {
        public List<PhoneNumber> PhoneNumbers { get; set; }
        public string Name { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            

            List<Student> studentList = new List<Student>()
            {
                new Student(){Id = 1, Name = "Birillo", Age = 18, DepartmentId = 1},
                new Student(){Id = 2, Name = "Daltanious", Age = 60, DepartmentId = 1},
                new Student(){Id = 3, Name = "Gordian", Age = 22, DepartmentId = 2},
                new Student(){Id = 4, Name = "Yattacan", Age = 54, DepartmentId = 3},
                new Student(){Id = 5, Name = "Paolo Brosio", Age = 33, DepartmentId = 4},
                new Student(){Id = 5, Name = "Paolo Brosio2", Age = 33, DepartmentId = 4},
                new Student(){Id = 5, Name = "Paolo Brosio3", Age = 33, DepartmentId = 4}
            };

            List<Department> departmentList = new List<Department>()
            {
                new Department() { Id = 1, Name = "History"},
                new Department() { Id = 2, Name = "Math"},
                new Department() { Id = 3, Name = "IT"},
                new Department() { Id = 4, Name = "Biology"},
            };

            // Esistono due sintassi per le query in LINQ: query syntax e method syntax
            // La prima viene tradotta nella seconda, ma entrambe sono ottimizzate
            // SCEGLIETE SEMPRE LA SINTASSI PER VOI PIU' LEGGIBILE

            /* where */

            // Query syntax
            var studentsOlderThan18 = from student in studentList where student.Age > 18 select student;

            // Method syntax
            var studentsYoungerThan22 = studentList.Where(student => student.Age <= 22);

            /* order by */

            // Query syntax
            var orderByAge = from student in studentList orderby student.Age select student;

            // Method syntax
            var orderByDepartmentName = departmentList
                .OrderBy(department => department.Name);

            /* group by */

            // Query syntax
            var groupByDepartment = from student in studentList group student by student.DepartmentId;

            // Method syntax
            groupByDepartment = studentList.GroupBy(student => student.DepartmentId);
            
            /* all */

            // Query syntax
            // ???

            // Method syntax
            var areAllHigherThan18 = studentList.All(student => student.Age > 18);

            /* any */

            var isThereAnyHigherThan18 = studentList.Any(student => student.Age > 18);

            var areAllHigherThan5AndAtLeastOne18 = studentList.All(student => student.Age > 5) &&
                                                   studentList.Any(student => student.Age == 18);

            /* aggregate */

            List<int> numbers = new List<int>(){1,2,3,4,5,6};

            var aggregateNumber = numbers.Aggregate((accumulator, next) => accumulator + next);

            var studentOne = studentList.Aggregate((accumulator, next) => new Student()
            {
                Id = accumulator.Id + next.Id,
                Name = $"{accumulator.Name} {next.Name} ",
                Age = (byte) (accumulator.Age + next.Age),
                DepartmentId = accumulator.DepartmentId + next.DepartmentId
            });

            /* concat */

            var secondStudentList = studentList;

            var concatList = studentList.Concat(secondStudentList);

            /* first */

            var firstStudentFromDep1 = studentList.First(student => student.DepartmentId == 1);

            /* first or default */

            var firstOrDefaultStudentFromDep6 = studentList.FirstOrDefault(student => student.DepartmentId == 6);

            /* last */

            var lastStudentFromDep1 = studentList.Last(student => student.DepartmentId == 1);

            /* last or default */

            var lastOrDefaultStudentFromDep6 = studentList.LastOrDefault(student => student.DepartmentId == 6);

            /* distinct */

            var agesNoDuplicates = studentList.Select(student => student.Age).Distinct();

            /* join */

            // Query syntax

            var innerJoinQuerySyntax = from student in studentList
                join department in departmentList
                    on student.DepartmentId equals department.Id
                select new
                {
                    StudentName = student.Name, Age = student.Age, DepartmentName = department.Name
                };

            // Method syntax

            var innerJoinMethodSyntax = studentList.Join( // Tabella student
                departmentList, // tabella department
                student => student.DepartmentId, // prima key coinvolta nella condizione di join
                department => department.Id, // seconda key coinvolta nella condizione di join
                (student, department) => // select
                new
                {
                    StudentName = student.Name,
                    Age = student.Age,
                    DepartmentName = department.Name
                });

            /* select many */

            List<Person> people = new List<Person>()
            {
                new Person(){
                    Name = "Metal Carter", 
                    PhoneNumbers = new List<PhoneNumber>()
                {
                    new PhoneNumber() { Number = "3336669999"},
                    new PhoneNumber() { Number = "3287771234"},
                }},
                new Person(){
                    Name = "Pippo Sowlo",
                    PhoneNumbers = new List<PhoneNumber>()
                    {
                        new PhoneNumber() { Number = "33461234449"},
                        new PhoneNumber() { Number = "3127775643"},
                        new PhoneNumber() { Number = "3470005111"}
                    }}
            };

            var phoneLists = people.Select(person => person.PhoneNumbers);
            var phoneNumbers = people.SelectMany(person => person.PhoneNumbers).ToList();
            var numberFound = phoneNumbers.FirstOrDefault(number => number.Number.Contains("666"));

            /* max */

            var oldestStudent = studentList.Max(student => student.Age);

            /* min */

            var youngestStudent = studentList.Min(student => student.Age);

            /* average */

            var average = studentList.Average(student => student.Age);

            /* single */

            //var singleDep1Student = studentList.Single(student => student.DepartmentId == 1); // Eccezione! Più di un
            // elemento corrisponde alla condizione

            /* single or default */

            // var singleOrDefaultDep1Student = studentList.SingleOrDefault(student => student.DepartmentId == 1);
            // Default se non lo trova, se me trova più di uno eccezione

            /* except */

            var numbers1 = new[] {1, 2, 3, 4};
            var numbers2 = new[] {1, 2, 4};

            var exceptList = numbers1.Except(numbers2);
        }
    }
}
