﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces.Interfaces;

namespace Interfaces.Abstract
{
    public abstract class Aircraft : IAircraft
    {
        protected Aircraft(byte engines)
        {
            Engines = engines;
            Model = "Unknown";
        }

        protected Aircraft(
            byte engines,
            string model,
            bool isTurboProp)
        {
            Engines = engines;
            Model = model;
            IsTurboProp = isTurboProp;
            IsTurboFan = !IsTurboProp;
        }

        public abstract string Model { get; set; }
        public abstract bool IsTurboFan { get; set; }
        public abstract bool IsTurboProp { get; set; }
        public abstract byte Engines { get; set; }

        public virtual void Land()
        {
            Console.WriteLine($"{Model} is landing");
        }

        public void Move()
        {
            Console.WriteLine($"{Model} is moving");
        }

        public void Stop()
        {
            Console.WriteLine($"{Model} is stopping");
        }

        public virtual void TakeOff()
        {
            Console.WriteLine($"{Model} is taking off");
        }
    }
}
