﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Interfaces
{
    public interface IWeapon
    {
        void Fire();
    }
}
