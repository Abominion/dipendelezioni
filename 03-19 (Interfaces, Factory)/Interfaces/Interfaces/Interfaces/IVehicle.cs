﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Channels;

namespace Interfaces.Interfaces
{
    public interface IVehicle // Interface Segregation ()
    {
        void Move();
        void Stop();
    }
}
