﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Interfaces
{
    public interface ICarrier : IVehicle
    {
        int MaxPassengers { get; set; }
        void BoardPassengers();
        void UnboardPassengers();
        void Break();
    }
}
