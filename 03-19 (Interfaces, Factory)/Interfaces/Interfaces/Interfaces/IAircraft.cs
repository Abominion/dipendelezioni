﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Interfaces.Interfaces
{
    public interface IAircraft : IVehicle
    {
        string Model { get; set; }
        bool IsTurboFan { get; set; }
        bool IsTurboProp { get; set; }
        byte Engines { get; set; }
        void TakeOff();
        void Land();
    }
}
