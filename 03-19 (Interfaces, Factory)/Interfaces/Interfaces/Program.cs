﻿using System;
using Interfaces.Abstract;
using Interfaces.Interfaces;

namespace Interfaces // Un'interfaccia è un contratto, che dunque definisce solo firme di
                     // metodi / comportamenti che la classe che la implementerà dovrà definire
{
    class Program
    {
        static void Main(string[] args)
        {
            ICarrier cotral = new Bus(60); // Liskov substitution principle
                                           // (classi concrete e interfacce devono poter
                                           // essere intercambiabili. Dunque un'astrazione può
                                           // sostituire la classe concreta che la implementa)
            cotral.BoardPassengers();
            cotral.UnboardPassengers();
            cotral.Break();

            ICarrier carrierBoeing747 = new Airliner(4, "Boeing 747", false, 250);
            carrierBoeing747.BoardPassengers();
            carrierBoeing747.UnboardPassengers();
            carrierBoeing747.Break();

            IAircraft aircraftBoeing747 = new Airliner(4, "Boeing 747", false, 250);
            aircraftBoeing747.TakeOff();
            aircraftBoeing747.Land();

            IAirliner abstractBoeing747 = new Airliner(4, "Boeing 747", false, 250);
            abstractBoeing747.Move();
            abstractBoeing747.Stop();
            abstractBoeing747.TakeOff();
            abstractBoeing747.Land();

            IWeapon f16Falcon = new Fighter(1, "F16 Falcon", false);
            f16Falcon.Fire();
        }
    }
}
