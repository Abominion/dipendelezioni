﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces.Abstract;
using Interfaces.Interfaces;

namespace Interfaces
{
    public class Fighter : Aircraft, IWeapon
    {
        public Fighter(byte engines) :base(engines)
        {
            
        }

        public Fighter(
            byte engines, 
            string model, 
            bool isTurboProp) : base(engines, model, isTurboProp)
        {
            
        }

        public override string Model { get; set; }
        public override bool IsTurboFan { get; set; }
        public override bool IsTurboProp { get; set; }
        public override byte Engines { get; set; }

        public void Fire()
        {
            Console.WriteLine($"{Model} engaging the enemy");
        }
    }
}
