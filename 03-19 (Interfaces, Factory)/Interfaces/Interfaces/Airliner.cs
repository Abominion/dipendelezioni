﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces.Abstract;
using Interfaces.Interfaces;

namespace Interfaces
{
    public class Airliner : IAirliner
    {
        public Airliner(
            byte engines,
            int maxPassengers)
        {
            MaxPassengers = maxPassengers;
        }

        public Airliner(
            byte engines,
            string model,
            bool isTurboProp,
            int maxPassengers)
        {
            MaxPassengers = maxPassengers;
        }

        public string Model { get; set; }
        public bool IsTurboFan { get; set; }
        public bool IsTurboProp { get; set; }
        public byte Engines { get; set; }

        public int MaxPassengers { get; set; }

        public void BoardPassengers()
        {
            Console.WriteLine($"{Model} is boarding passengers");
        }

        public void Break()
        {
            Console.WriteLine($"Ground control, we have a problem");
        }

        public void UnboardPassengers()
        {
            Console.WriteLine($"{Model} is unboarding passengers");
        }

        public void TakeOff()
        {
            Console.WriteLine($"{Model} is taking off on a runway");
        }

        public void Land()
        {
            Console.WriteLine($"{Model} is landing on a runway");
        }

        public void Move()
        {
            Console.WriteLine($"{Model} is moving");
        }

        public void Stop()
        {
            Console.WriteLine($"{Model} is stopping");
        }
    }
}
