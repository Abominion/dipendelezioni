﻿using System;
using System.Collections.Generic;
using System.Text;
using Interfaces.Interfaces;

namespace Interfaces
{
    public class Bus : ICarrier
    {
        public Bus(int maxPassengers)
        {
            MaxPassengers = maxPassengers;
        }

        public int MaxPassengers { get; set; }

        public void BoardPassengers()
        {
            Console.WriteLine("The bus is boarding passengers");
        }

        public void Move()
        {
            Console.WriteLine("The bus is moving");
        }

        public void Stop()
        {
            Console.WriteLine("The bus is stopping");
        }

        public void UnboardPassengers()
        {
            Console.WriteLine("The bus is unboarding passengers");
        }

        public void Break()
        {
            Console.WriteLine("The bus is broken");
        }
    }
}
