﻿using System;
using System.Collections.Generic;
using System.Text;
using Factory.Interfaces;

namespace Factory.Abstract
{
    public abstract class Factory : IFactory
    {
        public abstract ITransport Create();
    }
}
