﻿using System;
using System.Collections.Generic;
using System.Text;
using Factory.Interfaces;

namespace Factory.Concrete
{
    public class Truck : ITransport
    {
        public void Transport()
        {
            Console.WriteLine("Transporting goods by land");
        }
    }
}
