﻿using System;
using System.Collections.Generic;
using System.Text;
using Factory.Interfaces;


namespace Factory.Concrete
{
    public class TruckFactory : Abstract.Factory
    {
        public override ITransport Create()
        {
            return new Truck();
        }
    }
}
