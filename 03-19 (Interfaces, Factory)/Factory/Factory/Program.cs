﻿using System;
using Factory.Concrete;
using Factory.Interfaces;

namespace Factory // DP Creazionale. Fornisce un'interfaccia per creare oggetti
                  // in una superclasse delegando, però, la scelta degli oggetti
                  // da istanziare alle sottoclassi
{
    class Program
    {
        static void Main(string[] args)
        {
            IFactory factory = new TruckFactory();
            ITransport truck = factory.Create();
            truck.Transport();

            factory = new ShipFactory();
            ITransport ship = factory.Create();
            ship.Transport();

        }
    }
}
