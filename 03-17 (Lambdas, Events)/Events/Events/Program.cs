﻿using System;

namespace Events // Gli Eventi sono dei wrapper di delegati. Invocando un evento, vengono triggerati tutti i metodi
                // al suo interno. Si tratta di un design pattern noto come Publisher/Subscriber.
                // Un publisher è una classe che dichiara al suo interno un evento.
                // Una subscriber è una classe che registra uno o più dei suoi metodi all'interno di un evento.
                // Il/i metodo/metodi in questione sono detti handlers
{
    public delegate void EventHandler(object sender, string eventArg);

    class Publisher
    {
        public event EventHandler OnChange;

        public void Raise(string arg)
        {
            OnChange?.Invoke(this, arg); // Invocazione dell'evento
        }
    }

    class Handler1
    {
        public void Handle(object sender, string arg)
        {
            Console.WriteLine("Hello " + arg);
        }
    }

    class Handler2
    {
        public void Handle(object sender, string arg)
        {
            Console.WriteLine("Fuck " + arg);
        }
    }

    class Handler3
    {
        public void Handle(object sender, string arg)
        {
            Console.WriteLine("Yes " + arg);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Publisher publisher = new Publisher(); // Istanzio la publisher

            Handler1 handler1 = new Handler1(); // Istanzio i tre handler che andranno a registrarsi
                                                // all'evento della publisher
            Handler2 handler2 = new Handler2();
            Handler3 handler3 = new Handler3();

            publisher.OnChange += handler1.Handle; // Registro il primo handler
            publisher.OnChange += handler2.Handle; // Registro il secondo handler
            publisher.OnChange += handler3.Handle; // Registro il terzo handler

            publisher.Raise("Christian the Seeker"); // Invoco l'evento, causando il trigger di tutti i delegati
                                                     // registrati allo stesso
        }
    }
}
