﻿using System;
using System.Reflection;
using System.Threading.Channels;

namespace Lambdas // Le lambda rappresentano una sintassi semplificata per scrivere un delegato.
{
    public delegate void MyVoidDelegate(string name);

    public delegate int MyDelegate(int number1, int number2);

    class Program
    {
        static void Main(string[] args)
        {
            //MyVoidDelegate myVoidDelegate = (string param) => { Console.WriteLine($"Hello {param}"); }; // Lambda versione estesa
            MyVoidDelegate myVoidDelegate = param => Console.WriteLine($"Hello {param}"); // Versione contratta

            MyDelegate myDelegate = (num1, num2) => num1 + num2;

            Action actionDelegate = () => { Console.WriteLine("Hello, Stingeonnootsee!"); };

            Func<int, int, int, string> funcDelegate = (num1, num2, num3) => Convert.ToString(num1 + num2 + num3);

            Predicate<int> predicateDelegate = number =>
            {
                if (number == 1 || number == 2)
                    return true;

                for (int i = 0; i <= number / 2; i++)
                {
                    if (number % i == 0)
                        return false;
                }

                return true;
            };
        }

        class Dog
        {
            private string _name;

            public string Name
            {
                get => _name;
                set => _name = value;
            }

            public void Bark() => Console.WriteLine("Bark!");
        }
    }
}
