﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace MemoryGame
{
    public partial class Form1 : Form
    {
        private Label firstClicked = null;
        private Label secondClicked = null;

        private Random _random = new Random();

        private List<string> _icons = new List<string>()
        {
            "!", "!", "N", "N", ",", ",", "k", "k",
            "b", "b", "v", "v", "w", "w", "z", "z"
        };

        public Form1()
        {
            InitializeComponent();
            AssignIconsToSquares();
        }

        private void AssignIconsToSquares()
        {
            foreach (Control control in tableLayoutPanel1.Controls)
            {
                Label iconLabel = control as Label;
                if (iconLabel != null)
                {
                    int randomNumber = _random.Next(_icons.Count);
                    iconLabel.Text = _icons[randomNumber];
                    iconLabel.ForeColor = iconLabel.BackColor;
                    _icons.RemoveAt(randomNumber);
                }
            }
        }

        private void label_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
                return;

            Label clickedLabel = sender as Label; 

            if (clickedLabel != null)
            {
                if (clickedLabel.ForeColor == Color.Black) // Se entro qui, ho cliccato una casella già scoperta
                    return;

                if (firstClicked == null) // Se entro qui, la prima casella è coperta
                {
                    firstClicked = clickedLabel;
                    clickedLabel.ForeColor = Color.Black; // La scopro
                    return;
                }

                if (secondClicked != null) // Se entro qui, la seconda casella è scoperta
                    return;

                secondClicked = clickedLabel; // Se arrivo fin qui, significa che la prima casella è
                                              // scoperta e la seconda è coperta
                secondClicked.ForeColor = Color.Black; // Quindi la scopro

                if (firstClicked.Text == secondClicked.Text)
                {
                    firstClicked = null;
                    secondClicked = null;

                    CheckForWinner();
                    return;
                }


                timer1.Start();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();

            firstClicked.ForeColor = firstClicked.BackColor;
            secondClicked.ForeColor = secondClicked.BackColor;

            firstClicked = null;
            secondClicked = null;
        }

        private void CheckForWinner()
        {
            foreach (var control in tableLayoutPanel1.Controls)
            {
                Label iconLabel = control as Label;

                if (iconLabel != null)
                {
                    if (iconLabel.ForeColor == iconLabel.BackColor)
                        return;
                }
            }

            MessageBox.Show("Congratulations, you won :)", "Congratulations");
            Close();
        }
    }
}
