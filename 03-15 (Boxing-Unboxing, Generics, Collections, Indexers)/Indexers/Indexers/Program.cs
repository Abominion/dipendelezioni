﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Indexers // Gli indicizzatori sono una specie di overload delle [] per permetterti di
                   // accedere agli elementi di una collezione incapsulata
{
    class MyList<T>
    {
        private readonly T[] _elements = new T[100];

        public T this[int i] // Questo è un indicizzatore, sto definendo un comportamento nel
                             // caso in cui io faccia accesso a un oggetto di tipo MyList tramite parentesi quadre
        {
            get { return _elements[i]; }
            set { _elements[i] = value; }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new MyList<int>();

            numbers[0] = 666;

            Console.WriteLine(numbers[0]);
        }
    }
}
