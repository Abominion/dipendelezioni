﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generics // Potete rendere generiche classi, interfacce, struct e delegates
{
    class Box<T> // Per convention le lettere sono lettere singole: T sta per type, E per element, K per key, V per value
    {
        public T Field;
        public T Property { get; set; } // A questo punto al Field posso passare un numero, una stringa,
                                        // una persona, tutto ( perchè tutto deriva da Object)
        public T OpenBox()
        {
            return Property;
        }

        public void PrintMember(T member)
        {
            Console.WriteLine(member);
        }
    }

    class CustomDictionary<TKey, TValue>
    {
        TValue _value;

        public void Add(TKey key, TValue value)
        {
            // Stuff
        }
    }

    class Entity
    {
        public void ReplaceValue<T>(T a, T b) 
            where T: struct // T deve essere una struct, quindi un value type
            //where T : class // T deve essere un tipo riferimento
            //where T : new() // T deve essere l'istanza di un oggetto
            //where T : Persona // T deve essere l'istanza di quela specifica classe o una sua sottoclasse
            //where T : K // T deve essere dello stesso tipo di un altro generic
            //where T : unmanaged // T deve essere un'eccezione non gestita, quindi Exception o un sottotipo
            //where T : interface // T deve essere un'interfaccia o un sottotipo
            //where T : notnull // T non dev'essere un nullable type, quindi può essere un tipo valore

        {

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Box<int> box1 = new Box<int>();
            Console.WriteLine(box1.Property is int);

            Box<string> box2 = new Box<string>();
            Console.WriteLine(box2.Property is int);

            CustomDictionary<int, string> customDictionary = new CustomDictionary<int, string>();

            customDictionary.Add(1, "Ciao");

            var entity = new Entity();

            entity.ReplaceValue<int>(1,2);
            entity.ReplaceValue<double>(1,2);

            List<int> intList = new List<int>();
        }
    }
}
