﻿using System;

namespace BoxingUnboxing
{
    class Box // Una scatola può contenere qualunque cosa
    {
        public object Content { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Box box1 = new Box()
            {
                Content = "A string"
            };

            // Il content non è tipizzato, risulta essere di tipo object. Questa cosa si chiama boxing
            // Se volessi tipizzare il content dovrei castarlo

            string aString = (string) box1.Content; // Stiamo facendo unboxing. Da object convertiamo
                                                    // esplicitamente a un sottotipo

            // Il grosso rischio è che, se il cast non è valido, viene lanciata eccezione a runtime

            box1.Content = 1;

            aString = (string) box1.Content; // Eccezione!

            // Ecco perchè C# ci mette a disposizione uno strumento molto potente: i generics.
        }
    }
}
