﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace Collections
{
    class Box
    {
        public string Man { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            /* Collezioni non generiche e non thread safe */

            Console.WriteLine("*** Collezioni non generiche e non thread safe ***\n");

            // ArrayList (simile a un array, ma può essere utilizzata per storare oggetti di vario tipo)

            ArrayList arrayList = new ArrayList();
            var sentence = "Kebab is great";
            var randomNumber = 7;
            var today = DateTime.Now;

            arrayList.Add(sentence);
            arrayList.Add(randomNumber);
            arrayList.Add(today);

            Console.WriteLine("ArrayList: ");
            foreach (object element in arrayList)
            {
                Console.WriteLine($"- {element}");
            }

            // HashTable (simile a un ArrayList, ma può rappresentare gli oggetti come una
            // combinazione di coppie chiave valore)

            Hashtable hashTable = new Hashtable();
            hashTable.Add("ora", "orable");
            hashTable.Add("vb", "vb.net");
            hashTable.Add("cs", "cs.net");
            hashTable.Add(1, DateTime.Now);
            hashTable.Add("asp", "asp.net");
            //hashTable.Add("asp", "asp.net core"); // Le chiavi devono essere UNICHE

            Console.WriteLine("\nHashTable: ");
            foreach (DictionaryEntry dictionaryEntry in hashTable)
            {
                Console.WriteLine($"- {dictionaryEntry.Key}: {dictionaryEntry.Value}");
            }

            // SortedList (simile a un' HashTable ma ordina automaticamente gli elementi per chiave)

            Console.WriteLine("\nSortedList:");

            SortedList sortedList = new SortedList();
            sortedList.Add("d", "domodossola");
            sortedList.Add("c", "catania");
            sortedList.Add("a", "aquila");
            sortedList.Add("b", "bari");

            foreach (DictionaryEntry dictionaryEntry in sortedList)
            {
                Console.WriteLine($"- {dictionaryEntry.Key}: {dictionaryEntry.Value}");
            }

            // Stack (collezioni di tipo LIFO. Come una pila di piatti)

            Console.WriteLine("\nStack:");
            Stack stack = new Stack();

            stack.Push("Piatto 1");
            Console.WriteLine("Inserito piatto 1");
            stack.Push("Piatto 2");
            Console.WriteLine("Inserito piatto 2");
            stack.Push("Piatto 3");
            Console.WriteLine("Inserito piatto 3");
            stack.Push("Piatto 4");
            Console.WriteLine("Inserito piatto 4");

            Console.WriteLine($"Peeking top element: {stack.Peek()}"); // Restituisce l'oggetto in cima alla stack senza rimuoverlo

            while (stack.Count > 0)
            {
                Console.WriteLine($"Rimosso {stack.Pop()}");
            }

            // Queue (sono collezioni di tipo FIFO. Come la coda alle poste)

            Queue queue = new Queue();

            Console.WriteLine("\nQueue: ");
            queue.Enqueue("Customer 1");
            Console.WriteLine($"Entra il customer {queue.Peek()}");
            queue.Enqueue("Customer 2");
            Console.WriteLine($"Entra il customer Customer 2");
            queue.Enqueue("Customer 3");
            Console.WriteLine($"Entra il customer Customer 3");
            queue.Enqueue("Customer 4");
            Console.WriteLine($"Entra il customer Customer 4");

            //Console.WriteLine(queue[2]); // NO!

            while (queue.Count > 0)
            {
                Console.WriteLine($"Esce il {queue.Dequeue()}");
            }

            /* Collezioni generiche (no boxing e unboxing) e non thread safe */

            Console.WriteLine("\n *** Collezioni generiche e non thread safe");

            // List<T> (la regina delle collezioni. Si comporta come un array, ovvero contiene valori
            // di un determinato tipo, ma non ha dimensione fissa, bensì dinamica)
            
            //List<int> numbers = new List<int>();
            //numbers.Add(1);
            //numbers.Add(2);
            //numbers.Add(3);

            List<int> numbers = new List<int>() // Inizializzare una lista tramite initializer
            {
                12,100, 2,0,-4, 100
            };

            Console.WriteLine("\nList<T>:");

            foreach (var number in numbers)
            {
                Console.WriteLine($"- {number}");
            }

            numbers.Sort(); // Riordina gli elementi di una list

            Console.WriteLine();
            foreach (var number in numbers)
            {
                Console.WriteLine($"- {number}");
            }

            numbers.Reverse();

            Console.WriteLine();
            foreach (var number in numbers)
            {
                Console.WriteLine($"- {number}");
            }

            Console.WriteLine($"\nContiene -4: {numbers.Contains(-4)}");
            Console.WriteLine($"\nIndice di 0: {numbers.IndexOf(0)}");
            Console.WriteLine($"\nUltima posizione dove compare 100: {numbers.LastIndexOf(100)}");
            Console.WriteLine($"\nLunghezza della lista: {numbers.Count}");
            Console.WriteLine($"\nCapacità di una lista: {numbers.Capacity}");

            int[] trio = {600, 700, 800};
            numbers.AddRange(trio);

            numbers.Remove(100);
            numbers.RemoveRange(0, 2);
            numbers.RemoveAt(0);

            Console.WriteLine(numbers[0]); // Posso accedere agli elementi con le parentesi quadre

            var trioList = trio.ToList();
            var numbersArray = numbers.ToArray();
            var numbersHashset = numbers.ToHashSet();

            numbers.Clear();

            // Dictionary<K,V> (come le HashTable ma generiche)

            //Dictionary<string, int> demographics = new Dictionary<string, int>();
            //demographics.Add("Rome", 3_000_000);
            //demographics.Add("Tokyo", 10_000_000);
            //demographics.Add("Paris", 2_000_000);
            //demographics.Add("Rome", 3_000_000);

            

            Dictionary<string, int> demographics = new Dictionary<string, int>()
            {
                {"Rome",  6_000_000},
                {"Tokyo", 10_000_000 },
                { "Paris", 2_000_000 },
            };
            
            Console.WriteLine("\nDictionary<K,V>:");
            foreach (KeyValuePair<string, int> demographic in demographics)
            {
                Console.WriteLine($"- {demographic.Key}: {demographic.Value}");
            }

            Console.WriteLine($"Contains key Rome: {demographics.ContainsKey("Rome")}");
            Console.WriteLine($"Contains value 1: {demographics.ContainsValue(1)}");
            Console.WriteLine("Dictionary keys: ");
            foreach (string demographicsKey in demographics.Keys)
            {
                Console.WriteLine($"- {demographicsKey}");
            }

            Console.WriteLine(demographics["Tokyo"]);

            Console.WriteLine("Dictionary values: ");
            foreach (int demographicsValue in demographics.Values)
            {
                Console.WriteLine($"- {demographicsValue}");
            }

            // SortedList<K,T> (sono come le sorted list non generiche, mettono in ordine gli elementi per chiave)

            SortedList<string, int> consoleReleaseYears = new SortedList<string, int>();

            consoleReleaseYears.Add("PlayStation", 1994);
            consoleReleaseYears.Add("XBox", 2001);
            consoleReleaseYears.Add("SNES", 1990);
            consoleReleaseYears.Add("Odissey", 1972);

            Console.WriteLine("\nSortedList<K,V>:");
            foreach (KeyValuePair<string, int> keyValuePair in consoleReleaseYears)
            {
                Console.WriteLine($"- {keyValuePair.Key}: {keyValuePair.Value}");
            }

            // Stack<T> (stack ma generiche)

            Console.WriteLine("\nStack<T>:");
            Stack<string> stack2 = new Stack<string>();

            stack2.Push("Piatto 1");
            Console.WriteLine($"Inserito {stack2.Peek()}");
            stack2.Push("Piatto 2");
            Console.WriteLine($"Inserito {stack2.Peek()}");
            stack2.Push("Piatto 3");
            Console.WriteLine($"Inserito {stack2.Peek()}");
            stack2.Push("Piatto 4");
            Console.WriteLine($"Inserito {stack2.Peek()}");

            Console.WriteLine($"Peeking top element: {stack2.Peek()}");

            while (stack2.Count > 0)
            {
                Console.WriteLine($"Rimosso {stack2.Pop()}");
            }

            // Queue<T> (queue ma generiche)

            Queue<string> queue2 = new Queue<string>();

            Console.WriteLine("\nQueue<T>: ");
            queue2.Enqueue("Customer 1");
            Console.WriteLine($"Entra il customer {queue2.Peek()}");
            queue2.Enqueue("Customer 2");
            Console.WriteLine($"Entra il customer Customer 2");
            queue2.Enqueue("Customer 3");
            Console.WriteLine($"Entra il customer Customer 3");
            queue2.Enqueue("Customer 4");
            Console.WriteLine($"Entra il customer Customer 4");

            while (queue2.Count > 0)
            {
                Console.WriteLine($"Esce il {queue2.Dequeue()}");
            }

            // HashSet<T> (si tratta di una collezione che non ammette duplicati e
            // non tengono conto dell'ordine degli elementi)

            HashSet<string> cities = new HashSet<string>();
            cities.Add("Empoli");
            cities.Add("Detroit");
            cities.Add("Mosca");
            cities.Add("Atene");
            cities.Add("Ardea");

            Console.WriteLine("\nHashSet<T>:");
            foreach (var city in cities)
            {
                Console.WriteLine($"- {city}");
            }

            HashSet<Box> boxes = new HashSet<Box>();

            var box1 = new Box(){Man = "Alice"};
            var box2 = new Box(){Man = "in"};
            var box3 = new Box(){Man = "Chains"};
            var box4 = new Box(){Man = "Chains"};

            boxes.Add(box1);
            boxes.Add(box2);
            boxes.Add(box3);
            boxes.Add(box4); // Lo aggiunge, anche se il valore di Man è uguale a quello di box3,
                             // questo perchè gli hashset si basano su degli hashcode, degli identificativi
                             // validi solo all'interno degli HashSet

            foreach (var box in boxes)
            {
                Console.WriteLine($"- {box.Man} (hashcode: {box.GetHashCode()})");
            }

            // SortedSet<T> (Anche questo è un set, dunque no duplicati, ma mette in ordine gli elemnenti)

            SortedSet<string> sortedCities = new SortedSet<string>();

            sortedCities.Add("Empoli");
            sortedCities.Add("Detroit");
            sortedCities.Add("Osaka");
            sortedCities.Add("Delhi");
            sortedCities.Add("Bangkok");

            Console.WriteLine("\nSortedSet<T>:");
            foreach (var sortedCity in sortedCities)
            {
                Console.WriteLine($"- {sortedCity}");
            }

            // SortedDictionary<K,V> (come i Dictionary, ma riordina per chiave)

            SortedDictionary<string, int> ages = new SortedDictionary<string, int>()
            {
                {"Lorenzo", 1990},
                {"Federico", 1989},
                {"Davide", 2000}
            };

            Console.WriteLine("\nSortedDictionary<K,V>: ");
            foreach (var keyValuePair in ages)
            {
                Console.WriteLine($"- {keyValuePair.Key}: {keyValuePair.Value}");
            }

            /* Collezioni generiche e thread safe */

            // Servono a gestire la lettura/scrittura concorrente sulle collezioni

            // ConcurrentBag<T> // E' come una List
            // ConcurrentDictionary<K,V>
            // ConcurrentStack<T>
            // ConcurrentQueue<T>
            // BlockingCollection<T>
        }
    }
}
