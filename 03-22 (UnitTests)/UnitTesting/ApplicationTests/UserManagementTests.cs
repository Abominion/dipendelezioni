using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Application;
using Xunit;

namespace ApplicationTests
{
    public class UserManagementTests
    {
        private readonly UserManagement _userManagement;

        public UserManagementTests()
        {
            // Arrange
            _userManagement = new UserManagement();
        }

        [Fact] // Questo si chiama attribute, definisce il metodo come metodo di test
        public void GetAll_NoInput_ReturnsListOfUsers()
        {
            // Arrange (istanzio tutto ci� che mi serve per richiamare i metodi da testare)
            // var userManagement = new UserManagement();
            
            // Act (eseguo le logiche da testare)
            var users = _userManagement.GetAll();

            // Assert (valuto i risultati)
            Assert.NotEmpty(users);
        }

        [Fact]
        public void GetById_InputIsValid_ReturnsUser()
        {
            // Act
            var user = _userManagement.GetById(1);

            // Assert
            Assert.NotNull(user);
        }

        [Fact]
        public void GetById_InputIsLessThan1_Throws()
        {
            // Act + Assert
            Assert.Throws<Exception>(() => _userManagement.GetById(-1));
        }

        [Fact]
        public void GetById_NoUserFound_Throws()
        {
            // Act + Assert
            Assert.Throws<Exception>(() => _userManagement.GetById(999));
        }

        [Fact]
        public void Add_InputIsValid_ReturnsAddedUser()
        {
            using var transaction = new TransactionScope();

            var validUser = new User
            {
                Id = 0,
                BirthDate = DateTime.Now,
                FirstName = "Ugone",
                LastName = "Fragolone",
                Email = "proslayer666@rekt.it"
            };

            var addedUser = _userManagement.Add(validUser);

            var lastInserted = _userManagement.GetAll().LastOrDefault();

            Assert.NotEqual(0, addedUser.Id);
            Assert.Equal(lastInserted, validUser);
        }

        // ETC...
    }
}
