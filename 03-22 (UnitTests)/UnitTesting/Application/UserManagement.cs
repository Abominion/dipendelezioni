﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Application.Interfaces;
using Bogus;

namespace Application
{
    public class UserManagement : IUserManagement
    {
        private readonly List<User> _users;
        private int _identity = 1;

        public UserManagement()
        {
            var userFaker = new Faker<User>()
                .RuleFor(user => user.Id, faker => _identity++)
                .RuleFor(user => user.FirstName, faker => faker.Person.FirstName)
                .RuleFor(user => user.LastName, faker => faker.Person.LastName)
                .RuleFor(user => user.Email, faker => faker.Person.Email)
                .RuleFor(user => user.BirthDate, faker => faker.Person.DateOfBirth);

            _users = userFaker.Generate(20);
        }

        public User Add(User user)
        {
            if(user == null)
                throw new Exception("User can't be null");

            if(string.IsNullOrWhiteSpace(user.LastName))
                throw new Exception("Last name is required");

            user.Id = _identity++; // Assegno l'identity e la incremento

            _users.Add(user);

            return user; // Ritorno lo user col nuovo id
        }

        public bool DeleteById(int id)
        {
            if(id < 1)
                throw new Exception("Invalid id");

            var userToDelete = _users.FirstOrDefault(user => user.Id == id);

            if (userToDelete == null)
                throw new Exception("User to delete not found");

            var isDeleted = _users.Remove(userToDelete);

            return isDeleted;
        }

        public List<User> GetAll()
        {
            return _users;
        }

        public User GetById(int id)
        {
            if (id < 1)
                throw new Exception("Invalid id");

            var user = _users.FirstOrDefault(u => u.Id == id);

            if(user == null)
                throw new Exception("User not found");

            return user;
        }

        public bool Update(User user)
        {
            if(user == null)
                throw new Exception("User can't be null");

            if(string.IsNullOrWhiteSpace(user.LastName))
                throw new Exception("Last name is required");

            var userToUpdate = _users.FirstOrDefault(u => u.Id == user.Id);

            if(user == userToUpdate)
                throw new Exception("Updated user is the same as existing one");

            if(userToUpdate == null)
                throw new Exception("User to update not found");

            userToUpdate.FirstName = user.FirstName;
            userToUpdate.LastName = user.LastName;
            userToUpdate.BirthDate = user.BirthDate;
            userToUpdate.Email = user.Email;

            return true;
        }
    }
}
