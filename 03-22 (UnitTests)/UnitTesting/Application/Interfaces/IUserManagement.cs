﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces
{
    public interface IUserManagement
    {
        List<User> GetAll();
        User GetById(int id);
        bool Update(User user);
        User Add(User user);
        bool DeleteById(int id);
    }
}
