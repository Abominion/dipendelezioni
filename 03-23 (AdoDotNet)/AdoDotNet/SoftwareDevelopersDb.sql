USE [master]
GO
/****** Object:  Database [SoftwareDevelopers]    Script Date: 24/03/2021 12:51:51 ******/
CREATE DATABASE [SoftwareDevelopers]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SoftwareDevelopers', FILENAME = N'D:\SqlServer2017\MSSQL14.MSSQLSERVER\MSSQL\DATA\SoftwareDevelopers.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SoftwareDevelopers_log', FILENAME = N'D:\SqlServer2017\MSSQL14.MSSQLSERVER\MSSQL\DATA\SoftwareDevelopers_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [SoftwareDevelopers] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SoftwareDevelopers].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SoftwareDevelopers] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET ARITHABORT OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SoftwareDevelopers] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SoftwareDevelopers] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SoftwareDevelopers] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SoftwareDevelopers] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET RECOVERY FULL 
GO
ALTER DATABASE [SoftwareDevelopers] SET  MULTI_USER 
GO
ALTER DATABASE [SoftwareDevelopers] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SoftwareDevelopers] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SoftwareDevelopers] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SoftwareDevelopers] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SoftwareDevelopers] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SoftwareDevelopers', N'ON'
GO
ALTER DATABASE [SoftwareDevelopers] SET QUERY_STORE = OFF
GO
USE [SoftwareDevelopers]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 24/03/2021 12:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Genre](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GenreName] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SoftwareHouse]    Script Date: 24/03/2021 12:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoftwareHouse](
	[Id] [int] NOT NULL,
	[CompanyName] [varchar](50) NULL,
	[FoundationYear] [smallint] NULL,
 CONSTRAINT [PK__Software__3214EC077872A4A4] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Videogame]    Script Date: 24/03/2021 12:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Videogame](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NULL,
	[ReleaseYear] [smallint] NULL,
	[SoftwareHouseId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VideogameGenre]    Script Date: 24/03/2021 12:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideogameGenre](
	[GenreId] [int] NULL,
	[VideogameId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Videogame] ADD  CONSTRAINT [DF_Videogame_SoftwareHouseId]  DEFAULT (NULL) FOR [SoftwareHouseId]
GO
ALTER TABLE [dbo].[Videogame]  WITH CHECK ADD  CONSTRAINT [FK__Videogame__Softw__3B75D760] FOREIGN KEY([SoftwareHouseId])
REFERENCES [dbo].[SoftwareHouse] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Videogame] CHECK CONSTRAINT [FK__Videogame__Softw__3B75D760]
GO
ALTER TABLE [dbo].[VideogameGenre]  WITH CHECK ADD FOREIGN KEY([GenreId])
REFERENCES [dbo].[Genre] ([Id])
GO
ALTER TABLE [dbo].[VideogameGenre]  WITH CHECK ADD  CONSTRAINT [FK__Videogame__Video__3E52440B] FOREIGN KEY([VideogameId])
REFERENCES [dbo].[Videogame] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[VideogameGenre] CHECK CONSTRAINT [FK__Videogame__Video__3E52440B]
GO
USE [master]
GO
ALTER DATABASE [SoftwareDevelopers] SET  READ_WRITE 
GO
