﻿using System;
using System.Collections.Generic;
using System.Text;
using AdoDotNet.Repository.Entities;

namespace AdoDotNet.Repository.DTO
{
    public class SoftwareHouseWithGames // POCO: Plain Old CLR Object
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public short? FoundationYear { get; set; }
        public List<Videogame> Videogames { get; set; } = new List<Videogame>();
    }
}
