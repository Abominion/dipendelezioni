﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdoDotNet.Repository.Entities
{
    public class SoftwareHouse // POCO: Plain Old CLR Object
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public short? FoundationYear { get; set; }
    }
}
