﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdoDotNet.Repository.Entities
{
    public class Videogame
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public short ReleaseYear { get; set; }
    }
}
