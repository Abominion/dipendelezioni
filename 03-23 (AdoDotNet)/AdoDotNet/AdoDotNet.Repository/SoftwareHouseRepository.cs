﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using AdoDotNet.Repository.DTO;
using AdoDotNet.Repository.Entities;
using AdoDotNet.Repository.Interfaces;

namespace AdoDotNet.Repository
{
    public class SoftwareHouseRepository : ISoftwareHouseRepository
    {
        readonly string _connectionString;

        public SoftwareHouseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<SoftwareHouse> GetAll()
        {
            List<SoftwareHouse> softwareHouses = new List<SoftwareHouse>();

            try
            {
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString =
                        _connectionString;

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "SELECT * FROM SoftwareHouse";

                        connection.Open();

                        using (SqlDataReader sqlDataReader = command.ExecuteReader())
                        {
                            if (sqlDataReader.HasRows)
                            {
                                while (sqlDataReader.Read())
                                {
                                    SoftwareHouse softwareHouse = new SoftwareHouse
                                    {
                                        Id = sqlDataReader.GetInt32(0),
                                        Name = sqlDataReader.GetString(1),
                                        FoundationYear = sqlDataReader.IsDBNull(2) ? null 
                                            : (short?) sqlDataReader.GetInt16(2)
                                    };

                                    softwareHouses.Add(softwareHouse);
                                }
                            }
                        }
                    }
                }
            }
            #region Catches
            catch (InvalidCastException e)
            {
                Console.WriteLine(e);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            #endregion

            return softwareHouses;
        }

        public SoftwareHouse GetById(int id)
        {
            SoftwareHouse softwareHouse = null;

            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var command = new SqlCommand($"SELECT * FROM SoftwareHouse WHERE Id = {id}", connection))
                    {
                        connection.Open();

                        using (var sqlDataReader = command.ExecuteReader())
                        {
                            if (sqlDataReader.HasRows)
                            {
                                while (sqlDataReader.Read())
                                {
                                    softwareHouse = new SoftwareHouse
                                    {
                                        Id = (int) sqlDataReader["Id"],
                                        Name = sqlDataReader["CompanyName"].ToString(),
                                        FoundationYear = (short) sqlDataReader["FoundationYear"]
                                    };
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return softwareHouse;
        }

        public int Add(SoftwareHouse softwareHouse)
        {
            int rowsAffected = 0;

            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var command = new SqlCommand(
  $"INSERT INTO SoftwareHouse(CompanyName, FoundationYear) VALUES({softwareHouse.Name},{softwareHouse.FoundationYear})",
  connection)
                    )
                    {
                        connection.Open();

                        rowsAffected = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return rowsAffected;
        }

        public int Add(string companyName, short foundationYear)
        {
            int rowsAffected = 0;

            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var command = new SqlCommand(
                        $"INSERT INTO SoftwareHouse(CompanyName, FoundationYear) VALUES('{companyName}',{foundationYear})",
                        connection)
                    )
                    {
                        connection.Open();

                        rowsAffected = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return rowsAffected;
        }

        public int DeleteByName(string companyName)
        {
            int affectedRows = 0;

            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var command = new SqlCommand("DELETE FROM SoftwareHouse WHERE CompanyName = @companyName", connection))
                    {
                        connection.Open();

                        command.Parameters.AddWithValue("@companyName", companyName);

                        affectedRows = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return affectedRows;
        }

        public SoftwareHouseWithGames GetSoftwareHouseWithGames(int id)
        {
            var query =
                "SELECT * FROM SoftwareHouse AS S JOIN Videogame AS V ON V.SoftwareHouseId = S.Id WHERE S.Id = @id";
            SoftwareHouseWithGames softwareHouseWithGames = new SoftwareHouseWithGames();

            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);
                        connection.Open();

                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                reader.Read();

                                softwareHouseWithGames.Id = reader.GetInt32(0);
                                softwareHouseWithGames.CompanyName = reader.GetString(1);
                                softwareHouseWithGames.FoundationYear = reader.GetInt16(2);
                                
                                var game = new Videogame
                                {
                                    Id = reader.GetInt32(3),
                                    Title = reader.GetString(4),
                                    ReleaseYear = reader.GetInt16(5)
                                }; 

                                softwareHouseWithGames.Videogames.Add(game);

                                while (reader.Read())
                                {
                                    game = new Videogame
                                    {
                                        Id = reader.GetInt32(3),
                                        Title = reader.GetString(4),
                                        ReleaseYear = reader.GetInt16(5)
                                    };

                                    softwareHouseWithGames.Videogames.Add(game);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return softwareHouseWithGames;
        }
    }
}
