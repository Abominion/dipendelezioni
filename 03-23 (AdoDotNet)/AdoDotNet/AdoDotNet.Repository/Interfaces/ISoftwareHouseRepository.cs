﻿using System;
using System.Collections.Generic;
using System.Text;
using AdoDotNet.Repository.DTO;
using AdoDotNet.Repository.Entities;

namespace AdoDotNet.Repository.Interfaces
{
    public interface ISoftwareHouseRepository
    {
        IEnumerable<SoftwareHouse> GetAll();
        SoftwareHouse GetById(int id);
        int Add(SoftwareHouse softwareHouse);
        int Add(string companyName, short foundationYear);
        int DeleteByName(string companyName);
        SoftwareHouseWithGames GetSoftwareHouseWithGames(int id);
    }
}
