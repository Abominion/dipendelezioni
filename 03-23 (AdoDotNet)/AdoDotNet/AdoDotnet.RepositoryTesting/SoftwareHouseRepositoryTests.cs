using System;
using AdoDotNet.Repository;
using AdoDotNet.Repository.Interfaces;
using Xunit;

namespace AdoDotnet.RepositoryTesting
{
    public class SoftwareHouseRepositoryTests
    {
        readonly ISoftwareHouseRepository _softwareHouseRepository;

        public SoftwareHouseRepositoryTests()
        {
            // Arrange
            _softwareHouseRepository = new SoftwareHouseRepository(
                "Data Source=LAPTOP-809EEFU0;Initial Catalog=SoftwareDevelopers;Integrated security=true");
        }

        [Fact]
        public void GetAll_NoInput_ReturnsSoftwareHouseList()
        {
            // Act
            var softwareHouses = _softwareHouseRepository.GetAll();

            Assert.NotEmpty(softwareHouses);
        }

        [Fact]
        public void GetById_InputIsValid_ReturnsSoftwareHouse()
        {
            // Act
            var softwareHouse = _softwareHouseRepository.GetById(1);

            Assert.NotNull(softwareHouse);
        }

        [Fact]
        public void GetById_InputIsInvalid_ReturnsNull()
        {
            // Act
            var softwareHouse = _softwareHouseRepository.GetById(0);

            Assert.Null(softwareHouse);
        }

        [Fact]
        public void Add_InputIsValid_Returns1()
        {
            // Act
            var affectedRows = _softwareHouseRepository.Add("Cd Projekt Red", 1994);

            Assert.Equal(1, affectedRows);
        }

        [Fact]
        public void DeleteByName_InputIsValid_Returns1()
        {
            var affectedRows = _softwareHouseRepository.DeleteByName("'random' OR '1' = '1'");

            Assert.Equal(1, affectedRows);
        }

        [Fact]
        public void GetSoftwareHouseWithGames_InputIsValid_ReturnsSHWithGames()
        {
            var softwareHouseWithGames = _softwareHouseRepository.GetSoftwareHouseWithGames(1);

            Assert.Equal(1, softwareHouseWithGames.Id);
        }


    }
}
