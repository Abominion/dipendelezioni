﻿using System;

namespace StructsAndEnums
{
    public struct Book : IBook
    {
        public Book(string title, string author, int releaseYear)
        {
            Title = title;
            Author = author;
            ReleaseYear = releaseYear;
        }

        public string Title { get; set; }
        public string Author { get; set; }
        public int ReleaseYear;

        public void PrintBook()
        {
            Console.WriteLine($"{Title} {Author}");
        }

        public void PrintBook(string title)
        {
            Console.WriteLine($"{title} {Author}");
        }

        public void PublishBook()
        {
            Console.WriteLine($"Publishing {Title}");
        }
    }

    //public struct Encyclopedia : Book // Le struct non possono ereditare nè possono essere ereditate
    //{

    //}

    public class BookClass
    {
        public BookClass(string title, string author, int releaseYear)
        {
            Title = title;
            Author = author;
            ReleaseYear = releaseYear;
        }

        public string Title { get; set; }
        public string Author;
        public int ReleaseYear;
    }

    public interface IBook
    {
        void PublishBook();
    }

    public enum BookGenre // Un enum è un value type composto da costanti numeriche dotate di nomi.
    {
        Cooking = 20,
        Romance,
        Adventure = 30,
        Crime = 1,
        Horror,
        Poetry = 30,
    }

    public enum Giorno : byte // Puoi modificare il tipo dei valori numerici (devono essere numeri INTERI)
    {
        Lunedi = 1, Martedi, Mercoledi, Giovedi, Venerdi, Sabato, Domenica
    }

    class Program
    {
        static void Main(string[] args)
        {
            Book book1 = new Book("Le ricette coi carciofi di zia Erminia", "Zia Erminia", 2020);
            BookClass book2 = new BookClass("Cento e più ricette coi sassi", "Paolo Scandalo", 1310);

            Giorno oggi = Giorno.Giovedi;
            Console.WriteLine(oggi);
            Console.WriteLine((Giorno) 4);

            foreach (var value in Enum.GetValues(typeof(Giorno)))
            {
                Console.WriteLine($"{(byte) value} - {value}");
            }

            switch (oggi)
            {
                case Giorno.Lunedi:
                    Console.WriteLine("Primo lunedì della settimana");
                    break;
                case Giorno.Martedi:
                    Console.WriteLine("Secondo lunedì della settimana");
                    break;
                case Giorno.Mercoledi:
                    Console.WriteLine("Terzo lunedì della settimana");
                    break;
                case Giorno.Giovedi:
                    Console.WriteLine("Quarto lunedì della settimana");
                    break;
                case Giorno.Venerdi:
                    Console.WriteLine("Quinto lunedì della settimana");
                    break;
                case Giorno.Sabato:
                case Giorno.Domenica:
                    Console.WriteLine("Yippie");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            var dayOfWeek = DateTime.Today.DayOfWeek;
            var dayOfMonth = DateTime.Today.Day;

            switch (dayOfWeek)
            {
                case DayOfWeek.Sunday:
                    break;
                case DayOfWeek.Monday:
                    break;
                case DayOfWeek.Tuesday:
                    break;
                case DayOfWeek.Wednesday:
                    break;
                case DayOfWeek.Thursday when dayOfMonth % 2 == 0:
                    break;
                case DayOfWeek.Thursday when dayOfMonth % 2 != 0:
                    break;
                case DayOfWeek.Friday:
                    break;
                case DayOfWeek.Saturday:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
