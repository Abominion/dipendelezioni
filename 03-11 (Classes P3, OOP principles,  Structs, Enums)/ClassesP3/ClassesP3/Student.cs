﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassesP3
{
    // ASSEMBLY 1
    public partial class Student : Person // La keyword partial permette di splittare la definizione
        // di una classe tra file diversi
    {
        public string Department { get; set; }
        private DateTime _birthDate;
        public StudentsStatistics Statistics { get; set; }

        public Student(string firstName, string lastName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            Statistics = new StudentsStatistics(this);
        }

        public class StudentsStatistics
        {
            public StudentsStatistics(Student studentParam)
            {
                InnerStudent = studentParam;
            }

            public double GradeAverage { get; set; }
            public Student InnerStudent { get; set; }
        }
    }
}
