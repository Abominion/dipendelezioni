﻿using System;

namespace ClassesP3
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person() // Initializer (inizializzatore). Servono a inizializzare i campi
                                      // di un oggetto senza un costruttore parametrico
            {
                FirstName = "Ugo",
                LastName = "Sugo"
            };

            Console.WriteLine(person.FirstName);

            var ferrari = new // Anonymous type. Permettono di istanziare un oggetto senza averne definito il tipo.
                              // Dunque non hanno un tipo specifico, e si dice che siano dunque "tipi anonimi"
            {
                Brand = "Ferrari",
                Model = "Testa Rossa",
                MaxSpeed = 250,
                Wheels = 4,
                Pilot = person
            };

            var audi = new
            {
                Brand = "Audi",
                Model = "A3",
                MaxSpeed = 250,
                Wheels = 4
            };

            Console.WriteLine(ferrari.GetType());
            Console.WriteLine(audi.GetType());

            //var student1 = new Student()
            //{
            //    FirstName = "Ciccio",
            //    LastName = "Franco"
            //};

            //var student2 = new Student()
            //{
            //    FirstName = "Ciccio",
            //    LastName = "Graziani"
            //};

            //Console.WriteLine(student1.Equals(student2));

            var student = new Student("Albertone", "Ciambellone");

            //student.Statistics = new Student.StudentsStatistics(student);

            Console.WriteLine(student.Statistics.InnerStudent.FirstName);
            Console.WriteLine(student.Statistics.InnerStudent.LastName);
            Console.WriteLine(student.FirstName);
        }
    }
}
