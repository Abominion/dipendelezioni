﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassesP3
{
    // EXTENSION
    // ASSEMBLY 1
    public partial class Student
    {
        //public string Department { get; set; } // Membro già dichiarato nell'altro file
        public int Grade { get; set; }

        public bool Equals(Student other)
        {
            return this.FirstName == other.FirstName;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            
            return Equals((Student)obj);
        }
    }
}
