﻿using System;

namespace EncapsulationInheritancePolymorphism // Incapsulamento, ereditarietà e polimorfismo sono i tre pilastri
                                               // della programmazione a oggetti
{
    // Incapsulamento: la privatizzazione di un campo e il controllo degli accessi allo stesso
    // mediante degli appositi metodi get e set

    public abstract class MusicalInstrument
    {
        static MusicalInstrument()
        {
            Counter = 1;
        }

        public string Material { get; set; }
        public static int Counter { get; set; }
        public bool IsElectrical { get; set; }

        public abstract void Play();
    }

    // Ereditarietà: la capacità di una classe di ereditare le caratteristiche e i comportamenti di un'altra classe 
    // Si viene a creare una gerarchia di classi, in cui la classe ereditata è la
    // classe base, quelle che eredita è la classe derivata
    public class Guitar : MusicalInstrument
    {
        public string Name { get; set; }
        public int TotalChords { get; set; }

        public override void Play() // Polimorfismo: è la capacità di un oggetto di assumere diverse forme.
                                    // Ne conosciamo due tipi: override e overload.
                                    // L'override è la socrascrittura di un metodo della classe base contrassegnato
                                    // come abstract o virtual
        {
            Console.WriteLine($"{Name} is playing a solo");
        }

        public void Play(string song) // L'overload è la riproposizione di un metodo con lo stesso nome ma un diverso numero
                                      // di parametri e/o tipi di parametri diversi. Conta anche l'ordine dei parametri
                                      // nella parameters list. NON contano, ai fini dell'overload, il tipo di ritorno, eventuali modificatori
                                      // e il nome dei parametri in ingresso
        {
            Console.WriteLine($"{Name} is playing {song}");
        }

        public static int operator +(Guitar left, Guitar right) // Esempio di operator overloading, serve a riscrivere il comportamento
                                                                // di un operatore. Il metodo di overload deve essere static e public
        {
            return left.TotalChords + right.TotalChords;
        }

        public static int operator +(int left, Guitar right)
        {
            return left + right.TotalChords;
        }

        public static int operator +(Guitar left, int right)
        {
            return left.TotalChords + right;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            var guitar1 = new Guitar()
            {
                TotalChords = 6
            };

            var guitar2 = new Guitar()
            {
                TotalChords = 6
            };

            Console.WriteLine($"Total chords {guitar2 + (guitar1 + guitar2 + guitar2)}");
        }


    }
}
