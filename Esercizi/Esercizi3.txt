Programma 1

Scrivere un programma che stampi a console l'elenco dei file a partire da una cartella e scendendo in 
ogni sottocartella. Formattate le stampe a vostro piacimento

Programma 2

Scrivi un programma in grado di chiedere all'utente di inserire due stringhe, scrivere ognuna su un file diverso e di 
confrontare, dopo averlo letto dai file, il loro contenuto per comunicare se essi sono uguali o meno. Il programma NON dovrà prendere
in considerazione gli spazi bianchi, quindi "Ciao a tutti" e "c i ao     at   utti" saranno considerati due contenuti uguali.

