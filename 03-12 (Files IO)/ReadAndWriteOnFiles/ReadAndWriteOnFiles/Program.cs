﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security;
using System.Text;

namespace ReadAndWriteOnFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\Users\Abominion\Desktop";
            string fileName = "dipendeFile.txt";

            //if (File.Exists($@"{path}\{fileName}"))  // File è un helper per avere info sui file o compiere operazioni sugli stessi
            //{
            //    Console.WriteLine($"Il file {fileName} esiste nel percorso {path}");
            //}
            //else
            //{
            //    File.Create($@"{path}\{fileName}");
            //}

            string completePath = Path.Combine(path, fileName);

            if (File.Exists(completePath))
            {
                Console.WriteLine(
                    $"Il file {Path.GetFileName(completePath)} esiste nel percorso {Path.GetDirectoryName(completePath)}");
            }
            else
            {
                File.Create(completePath);
            }

            /* File e Directory hanno anche altri metodi utili per copiare un file o una cartella, spostartla, eliminarla ...*/

            // Scrivere su file in blocco:

            var sentence = "Pippo Franco ama le mandorle";
            var logFile = "logFile.txt";
            var logCompletePath = Path.Combine(path, logFile);

            if (File.Exists(completePath))
            {
                try // Il blocco try catch permette di catturare e gestire le eccezioni
                {
                    File.WriteAllText(completePath, sentence);

                    File.WriteAllText(logCompletePath,
                        $"{DateTime.Now} - Succesfully written {sentence} in {completePath}");
                }

                #region Catches 

                catch (SecurityException exception
                    ) // Per una gestione granulare delle eccezioni, è necessario catchare le
                    // stesse dalla più specifica alla meno specifica (Exception)
                {
                    File.WriteAllText(logCompletePath, exception.Message);
                    File.AppendAllText(logCompletePath, exception.StackTrace);
                }
                catch (NotSupportedException exception)
                {
                    File.WriteAllText(logCompletePath, exception.Message);
                    File.AppendAllText(logCompletePath, exception.StackTrace);
                }
                catch (UnauthorizedAccessException exception)
                {
                    File.WriteAllText(logCompletePath, exception.Message);
                    File.AppendAllText(logCompletePath, exception.StackTrace);
                }
                catch (DirectoryNotFoundException exception)
                {
                    File.WriteAllText(logCompletePath, exception.Message);
                    File.AppendAllText(logCompletePath, exception.StackTrace);
                }
                catch (PathTooLongException exception)
                {
                    File.WriteAllText(logCompletePath, exception.Message);
                    File.AppendAllText(logCompletePath, exception.StackTrace);
                }
                catch (IOException exception)
                {
                    File.WriteAllText(logCompletePath, exception.Message);
                    File.AppendAllText(logCompletePath, exception.StackTrace);
                }
                catch (ArgumentNullException exception)
                {
                    File.WriteAllText(logCompletePath, exception.Message);
                    File.AppendAllText(logCompletePath, exception.StackTrace);
                }
                catch (ArgumentException exception)
                {
                    File.WriteAllText(logCompletePath, exception.Message);
                    File.AppendAllText(logCompletePath, exception.StackTrace);
                }
                catch (Exception exception) // Tutte le eccezioni derivano dalla classe Exception
                {
                    File.WriteAllText(logCompletePath, exception.Message);
                    File.AppendAllText(logCompletePath, exception.StackTrace);
                }

                #endregion
            }

            // Leggere da file in blocco

            if (File.Exists(completePath))
            {
                try
                {
                    string fileContent = File.ReadAllText(completePath); // Legge blocchi di dati quindi non
                                                                         // posso operare su singole linee
                    Console.WriteLine(fileContent);
                }
                catch (Exception exception)
                {
                    File.WriteAllText(logCompletePath, exception.Message);
                    File.AppendAllText(logCompletePath, exception.StackTrace);
                }
                finally // Il blocco finally viene SEMPRE richiamato alla fine di un blocco try catch
                {
                    Console.WriteLine("End of try catch");
                }
            }

            // Scrivere su file linea per linea (andando automaticamente a capo dopo ogni linea)

            var names = new[] {"Pippo", "Peppe", "Pappa", "Peppa"};

            try
            {
                File.WriteAllLines(completePath, names);
            }
            catch (Exception exception)
            {
                File.WriteAllText(logCompletePath, exception.Message);
                File.AppendAllText(logCompletePath, exception.StackTrace);
            }

            // Concatenare su file linea per linea

            var newNames = new[] { "Simone", "Federico", "Andrea", "Astrid", "Riccardo", "Massimiliano", "Davide" };

            try
            {
                File.AppendAllLines(completePath, newNames);
            }
            catch (Exception exception)
            {
                File.WriteAllText(logCompletePath, exception.Message);
                File.AppendAllText(logCompletePath, exception.StackTrace);
            }

            // Leggere su file linea per linea

            try
            {
                var completeNamesArray = File.ReadAllLines(completePath);
                Console.WriteLine(string.Join(" ", completeNamesArray));
            }
            catch (Exception exception)
            {
                File.WriteAllText(logCompletePath, exception.Message);
                File.AppendAllText(logCompletePath, exception.StackTrace);
            }

            // Scrivere su file usando memoria buffer

            var colors = new[] {"Rosso", "Giallo", "Rosa", "Nero", "Blu"};

            try
            {
                using (StreamWriter writer = new StreamWriter(completePath, true))
                {
                    foreach (var color in colors)
                    {
                        writer.WriteLine(color);
                    }
                }
            }
            catch (Exception exception)
            {
                File.WriteAllText(logCompletePath, exception.Message);
                File.AppendAllText(logCompletePath, exception.StackTrace);
            }

            // Leggere da file usando la memoria buffer

            try
            {
                using (StreamReader reader = new StreamReader(completePath))
                {
                    string line;

                    while ((line = reader.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                    }
                }
            }
            catch (Exception exception)
            {
                File.WriteAllText(logCompletePath, exception.Message);
                File.AppendAllText(logCompletePath, exception.StackTrace);
            }

            using (StreamWriter writer = File.CreateText(completePath))  /* Inizializzazione alternativa di StreamWriter, CreateText restituisce uno StreamWriter */
            {
                writer.WriteLine("Ciao a");
            }
            using (StreamWriter writer = File.AppendText(completePath)) /* Appende al file quel testo */
            {
                writer.WriteLine(" tutti");
            }

            using (StreamReader reader = File.OpenText(completePath)) /* Alternativa al reader come sopra */
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
            }

            using (FileStream fileStream = new FileStream(completePath, FileMode.Append))
            {
                string sentence2 = "Signora c'è un uomo per lei";

                byte[] bytesSentence = Encoding.ASCII.GetBytes(sentence2);

                fileStream.Write(bytesSentence);
            }

            // Leggere tutti i file da una directory

            foreach (string file in Directory.GetFiles(path))
            {
                Console.WriteLine(file);
            }

            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            
            foreach (FileInfo fileInfo in directoryInfo.GetFiles())
            {
                Console.WriteLine($"Name: {fileInfo.Name} | Size: {fileInfo.Length} | Last accessed: {fileInfo.LastAccessTime}");
            }
        }
    }
}
