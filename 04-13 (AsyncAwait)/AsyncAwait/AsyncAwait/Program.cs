﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace AsyncAwait
{
    class Program
    {
        static void MethodA()
        {
            Console.WriteLine("Starting Method A...");
            Thread.Sleep(3000);
            Console.WriteLine("Finished Method A.");
        }

        static void MethodB()
        {
            Console.WriteLine("Starting Method B...");
            Thread.Sleep(2000);
            Console.WriteLine("Finished Method B.");
        }

        static void MethodC()
        {
            Console.WriteLine("Starting Method C...");
            Thread.Sleep(1000);
            Console.WriteLine("Finished Method C.");
        }

        static async Task<int> AsyncMethod()
        {
            Task<int> task = new Task<int>(() =>
            {
                Thread.Sleep(3000);
                return 100;
            }); // Creo un task con dentro un'operazione time consuming

            task.Start(); // Stacco il thread

            Thread.Sleep(2000); // Simulo altre operazioni

            return await task; // Attendo il return del task
        }

        static int SyncMethod()
        {
            Thread.Sleep(3000); // Operazioni time consuming

            var number = 100;

            Thread.Sleep(1000); // Operazioni time consuming

            return number;
        }

        static async Task Main(string[] args)
        {
            var timer = Stopwatch.StartNew();

            /* Sync */

            //Console.WriteLine("Running methods synchronously on one thread.");
            //MethodA();
            //MethodB();
            //MethodC();

            /* Async */

            //Console.WriteLine("Running methods asynchronously on several threads");

            //Task taskA = new Task(MethodA);
            //taskA.Start();

            //Task taskB = Task.Factory.StartNew(MethodB);

            //Task taskC = Task.Run(MethodC);

            //Task[] tasks = {taskA, taskB, taskC};

            //Task.WaitAll(tasks);

            int myNumber = await AsyncMethod();

            Console.WriteLine(myNumber);
            Console.WriteLine($"{timer.ElapsedMilliseconds:#,##0}ms elapsed");
        }
    }
}
