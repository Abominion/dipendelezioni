﻿using System;
using System.Collections.Generic;
using System.Text;
using DomainCenteredApp.Interfaces.Services;
using DomainCenteredApp.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace DomainCenteredApp.Business.Tests
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IMediaFilesService, MediaFilesService>();
        }
    }
}
