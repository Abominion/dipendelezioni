﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Xunit.Sdk;

namespace DomainCenteredApp.Business.Tests.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CleanupFilesAfterTest : BeforeAfterTestAttribute
    {
        public override void After(MethodInfo methodUnderTest)
        {
            var testDirectory =
                @"D:\GitRepos\dipendelezioni\03-30 (DomainCenteredApp)\DomainCenteredApp\DomainCenteredApp.Mvc\wwwroot\assets\test\";

            if (!Directory.Exists(testDirectory))
                return;

            Directory.Delete(testDirectory
                , true);
        }
    }
}
