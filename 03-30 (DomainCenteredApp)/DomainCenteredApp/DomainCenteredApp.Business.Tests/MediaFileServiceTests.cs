using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using DomainCenteredApp.Business.Tests.Filters;
using DomainCenteredApp.Interfaces.Services;
using DomainCenteredApp.Services;
using Xunit;

namespace DomainCenteredApp.Business.Tests
{
    public class MediaFileServiceTests
    {
        //private readonly string TestFilePath = $"{Directory.GetCurrentDirectory()}/assets/test/test.jpg";
        private readonly string TestFilePath =
            @"D:\GitRepos\dipendelezioni\03-30 (DomainCenteredApp)\DomainCenteredApp\DomainCenteredApp.Mvc\wwwroot\assets\test\test.jpg";
        private readonly IMediaFilesService _mediaFilesService;

        public MediaFileServiceTests(IMediaFilesService mediaFilesService)
        {
            _mediaFilesService = mediaFilesService;
        }

        [Fact]
        [CleanupFilesAfterTest]
        public void SaveFile_InputIsValid_ReturnsTrue()
        {
            IFormFile file = null;
            bool hasBeenWritten = false;
            Guid productId = Guid.NewGuid();

            try
            {
                using (var memoryStream = new MemoryStream(File.ReadAllBytes(TestFilePath).ToArray()))
                {
                    file = new FormFile(
                        memoryStream, 
                        0, 
                        memoryStream.Length, 
                        "test", 
                        TestFilePath.Split(@"\").Last());

                    hasBeenWritten = _mediaFilesService.SaveFile("images", file, productId);
                }

                Assert.True(hasBeenWritten);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
