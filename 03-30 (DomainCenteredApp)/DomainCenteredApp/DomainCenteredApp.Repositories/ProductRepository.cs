﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using DomainCenteredApp.Context;
using DomainCenteredApp.Context.Entities;
using DomainCenteredApp.Domain;
using DomainCenteredApp.Interfaces.Repositories;
using DomainCenteredApp.Mapper;

namespace DomainCenteredApp.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly ProductDbContext _ctx;
        private readonly IMapper _mapper;

        public ProductRepository(ProductDbContext ctx, IMapper mapper)
        {
            _ctx = ctx;
            _mapper = mapper;
        }

        public bool Add(ProductDomain product)
        {
            var productEntity = _mapper.Map<Product>(product); // Map<> in caso di oggetti singoli

            _ctx.Products.Add(productEntity);

            var addResult = _ctx.SaveChanges() == 1;

            return addResult;
        }

        public List<ProductDomain> GetAll()
        {
            var productEntities = _ctx.Products;

            //var productDomainList = productEntities
            //    .Select(product => CustomProductMapper.FromEntityToDomain(product))
            //    .ToList();

            var productDomainList = _mapper
                .ProjectTo<ProductDomain>(productEntities) // ProjectTo in caso di IQueryable, dunque collezioni di oggetti
                .ToList();

            return productDomainList;
        }


    }
}
