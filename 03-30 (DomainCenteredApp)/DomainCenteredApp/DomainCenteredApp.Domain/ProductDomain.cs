﻿using System;
using System.Collections.Generic;
using DomainCenteredApp.Domain.Enum;

namespace DomainCenteredApp.Domain
{
    public class ProductDomain
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IList<ColorEnum> Colors { get; set; }
        public string Description { get; set; }
        public double Weight { get; set; }
        public bool IsFragile { get; set; }
        public CategoryEnum Category { get; set; }
        public string Image { get; set; }
        public string Pdf { get; set; }
        public DateTime InsertionDate { get; set; }
    }
}
