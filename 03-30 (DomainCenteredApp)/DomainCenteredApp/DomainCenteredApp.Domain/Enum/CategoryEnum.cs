﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCenteredApp.Domain.Enum
{
    public enum CategoryEnum : byte
    {
        None,
        Software,
        Hardware,
        Kitchenware,
        Books,
        Furniture,
        Clothes
    }
}
