﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCenteredApp.Domain.Enum
{
    public enum ColorEnum
    {
        Unavailable,
        White,
        Black,
        Red,
        Green,
        Blue,
        Yellow,
        Pink
    }
}
