﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainCenteredApp.Context.Entities;
using DomainCenteredApp.Domain;
using DomainCenteredApp.Domain.Enum;

namespace DomainCenteredApp.Mapper
{
    public class CustomProductMapper
    {
        public static ProductDomain FromEntityToDomain(Product product)
        {
            var productDomain = new ProductDomain();

            productDomain.Id = product.Id;
            productDomain.Description = product.Description;
            productDomain.Image = product.Image;
            productDomain.Pdf = product.Pdf;
            productDomain.InsertionDate = product.InsertionDate;
            productDomain.IsFragile = product.IsFragile;
            productDomain.Name = product.Name;
            productDomain.Weight = product.Weight;

            productDomain.Category = Enum
                .GetValues(typeof(CategoryEnum))
                .Cast<byte>()
                .Contains(product.Category) ? (CategoryEnum) product.Category : CategoryEnum.None;

            //productDomain.Category = Array.BinarySearch(Enum.GetValues(typeof(CategoryEnum)), product.Category) >= 0
            //    ? (CategoryEnum) product.Category
            //    : CategoryEnum.None;
            
            //var colors = product.Colors.Split(',').ToList(); // Lista di stringhe
            //foreach (var color in colors) // Cicla su stringhe
            //{
            //    ColorEnum colorEnum = (ColorEnum) System.Enum.Parse(typeof(ColorEnum), color); // Parsa da string a ColorEnum
            //    productDomain.Colors.Add(colorEnum); // Aggiunge alla lista
            //}

            productDomain.Colors = product
                .Colors
                .Split(',')
                .Select(stringColor =>
                {
                    var isValidColor = Enum.IsDefined(typeof(ColorEnum), byte.TryParse(stringColor, out byte result)
                        ? result
                        : -1);

                    return isValidColor ? (ColorEnum) result : ColorEnum.Unavailable;
                }).ToList();
                
            
            return productDomain;
        }
    }
}
