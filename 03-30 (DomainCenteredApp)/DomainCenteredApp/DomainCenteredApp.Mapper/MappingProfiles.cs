﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using DomainCenteredApp.Context.Entities;
using DomainCenteredApp.Domain;
using DomainCenteredApp.Domain.Enum;

namespace DomainCenteredApp.Mapper
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Product, ProductDomain>()
                .ForMember(destination => destination.Category,
                    options => options.MapFrom(source => ToCategoryEnum(source.Category)))
                .ForMember(destination => destination.Colors,
                    options => options.MapFrom(source => ToColorEnums(source.Colors)
                    ));

            CreateMap<ProductDomain, Product>()
                .ForMember(destination => destination.Category,
                    options => options.MapFrom(source => (byte) source.Category))
                .ForMember(destination => destination.Colors,
                    options => options.MapFrom(source => ColorsToString(source.Colors)
                    ));
        }

        private static string ColorsToString(IEnumerable<ColorEnum> colors)
        {
            var colorsStringList = colors.Select(color => ((byte) color).ToString());

            return string.Join(',', colorsStringList);
        }

        private static CategoryEnum ToCategoryEnum(byte categoryByte)
        {
            return Enum
                .GetValues(typeof(CategoryEnum))
                .Cast<byte>()
                .Contains(categoryByte) ? (CategoryEnum)categoryByte : CategoryEnum.None;
        }

        private static List<ColorEnum> ToColorEnums(string colors)
        {
            var colorList = colors
                .Split(',')
                .Select(stringColor =>
                {
                    var isValidColor = Enum.IsDefined(typeof(ColorEnum), byte.TryParse(stringColor, out byte result)
                        ? result
                        : -1);

                    return isValidColor ? (ColorEnum)result : ColorEnum.Unavailable;
                }).ToList();

            return colorList;
        }
    }
}
