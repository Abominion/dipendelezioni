﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DomainCenteredApp.Context.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Colors = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Weight = table.Column<double>(nullable: false),
                    IsFragile = table.Column<bool>(nullable: false),
                    Category = table.Column<byte>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Pdf = table.Column<string>(nullable: true),
                    InsertionDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Product");
        }
    }
}
