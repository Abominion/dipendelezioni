﻿using System;
using System.Collections.Generic;
using System.Text;
using DomainCenteredApp.Context.Entities;
using Microsoft.EntityFrameworkCore;

namespace DomainCenteredApp.Context
{
    public class ProductDbContext : DbContext
    {
        public ProductDbContext() { }

        public ProductDbContext(DbContextOptions<ProductDbContext> options) : base(options)
        {

        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=LAPTOP-809EEFU0;Database=ProductDb;Trusted connection=true");
        //}

        public virtual DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Product>()
                .ToTable("Product");
        }
    }
}
