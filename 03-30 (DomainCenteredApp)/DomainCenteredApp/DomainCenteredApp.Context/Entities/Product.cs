﻿using System;
using System.Collections.Generic;

namespace DomainCenteredApp.Context.Entities
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Colors { get; set; }
        public string Description { get; set; }
        public double Weight { get; set; }
        public bool IsFragile { get; set; }
        public byte Category { get; set; }
        public string Image { get; set; }
        public string Pdf { get; set; }
        public DateTime InsertionDate { get; set; }
    }
}
