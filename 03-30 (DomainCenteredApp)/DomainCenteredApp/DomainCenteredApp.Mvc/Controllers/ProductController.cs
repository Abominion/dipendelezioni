﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainCenteredApp.Domain;
using DomainCenteredApp.Domain.Enum;
using DomainCenteredApp.Interfaces.Services;
using DomainCenteredApp.Mvc.Models;

namespace DomainCenteredApp.Mvc.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        private readonly IMediaFilesService _mediaFileService;

        public ProductController(IProductService productService, IMediaFilesService mediaFileService)
        {
            _productService = productService;
            _mediaFileService = mediaFileService;
        }

        [HttpGet]
        public IActionResult ShowProducts()
        {
            var products = _productService
                .GetAllProducts()
                .Select(product => new ProductLight(product));
            
            //var mockProducts = new List<ProductLight>
            //{
            //    new ProductLight
            //    {
            //        Category = CategoryEnum.Hardware.ToString(),
            //        Name = "Trust Keyboard GXT 865",
            //        Image = "/keyboard.jpg"
            //    },
            //    new ProductLight
            //    {
            //        Category = CategoryEnum.Clothes.ToString(),
            //        Name = "Ugly as fuck pijama",
            //        Image = "/pijama.jpg"
            //    },
            //    new ProductLight
            //    {
            //        Category = CategoryEnum.Kitchenware.ToString(),
            //        Name = "Dried tomatoes",
            //        Image = "/dried-tomatoes.jpg"
            //    }
            //};

            return View(products);
        }

        [HttpGet]
        public IActionResult CreateProduct()
        {
            var product = new ProductWithFiles();
            
            return View(product);
        }

        [HttpPost]
        public IActionResult CreateProduct(ProductWithFiles product)
        {
            if (!ModelState.IsValid)
                return View(product);

            try
            {
                var productId = _productService.AddWithGuid(product.ToProductDomain());

                if (productId == Guid.Empty)
                    throw new Exception("An error occurred while adding a product");

                var imageHasBeenWritten = _mediaFileService.SaveFile("images", product.Image, productId);
                // TODO Aggiornare il prodotto con il nome dell'immagine appena caricata
                var pdfHasBeenWritten = _mediaFileService.SaveFile("pdf", product.Pdf, productId);
                // TODO Aggiornare il prodotto con il nome del pdf appena caricato

                if(!imageHasBeenWritten || !pdfHasBeenWritten)
                    throw new Exception("An error occurred while saving media files");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "An error occurred while saving the product. Try again");
                return View(product);
            }

            return RedirectToAction("ShowProducts");
        }
    }
}
