﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainCenteredApp.Domain;

namespace DomainCenteredApp.Mvc.Models
{
    public class ProductLight
    {
        public ProductLight()
        {
            
        }

        public ProductLight(ProductDomain productDomain)
        {
            Id = productDomain.Id;
            Name = productDomain.Name;
            Category = productDomain.Category.ToString();
            Image = productDomain.Image;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Category { get; set; }
    }
}
