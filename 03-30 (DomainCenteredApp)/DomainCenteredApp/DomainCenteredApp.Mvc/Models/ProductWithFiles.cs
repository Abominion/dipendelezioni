﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DomainCenteredApp.Domain;
using DomainCenteredApp.Domain.Enum;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;

namespace DomainCenteredApp.Mvc.Models
{
    public class ProductWithFiles
    {
        public ProductWithFiles()
        {
        }
        [Required]
        public string Name { get; set; }
        public IList<ColorEnum> Colors { get; set; }
        public string Description { get; set; }
        public double Weight { get; set; }
        public bool IsFragile { get; set; }
        public CategoryEnum Category { get; set; }
        public IFormFile Image { get; set; }
        public IFormFile Pdf { get; set; }

        public ProductDomain ToProductDomain()
        {
            var product = new ProductDomain
            {
                Category = Category,
                Colors = Colors,
                Name = Name,
                Description = Description,
                InsertionDate = DateTime.Now,
                IsFragile = IsFragile,
                Weight = Weight,
                Image = Image.FileName,
                Pdf = Pdf.FileName
            };

            return product;
        }
    }

    //public class ImageFormatAttribute : ValidationAttribute
    //{
    //    public override bool IsValid(object value)
    //    {
    //        var file = value as IFormFile;

    //        if (file is null)
    //            return false;

    //        if (Path.GetExtension(file.FileName) == ".jpg")
    //            return true;

    //        return false;
    //    }
    //}
}
