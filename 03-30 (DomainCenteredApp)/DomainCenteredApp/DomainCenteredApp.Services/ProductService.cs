﻿using DomainCenteredApp.Interfaces;
using System;
using System.Collections.Generic;
using DomainCenteredApp.Domain;
using DomainCenteredApp.Interfaces.Repositories;
using DomainCenteredApp.Interfaces.Services;

namespace DomainCenteredApp.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _repo;

        public ProductService(IProductRepository repo)
        {
            _repo = repo;
        }

        public bool Add(ProductDomain product)
        {
            if(product == null)
                throw new Exception("Product can't be null");

            if(product.Weight > 100)
                throw new Exception("Products can't weigh over 100kg");

            product.Id = Guid.NewGuid();

            var isAdded = _repo.Add(product);

            return isAdded;
        }

        public Guid AddWithGuid(ProductDomain product)
        {
            if (product == null)
                throw new Exception("Product can't be null");

            if (product.Weight > 100)
                throw new Exception("Products can't weigh over 100kg");

            product.Id = Guid.NewGuid();

            var hasBeenAdded = _repo.Add(product);

            return hasBeenAdded ? product.Id : Guid.Empty;
        }

        public List<ProductDomain> GetAllProducts()
        {
            var products = _repo.GetAll();

            return products;
        }
    }
}
