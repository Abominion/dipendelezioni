﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Unicode;
using DomainCenteredApp.Interfaces.Services;
using Microsoft.AspNetCore.Http;

namespace DomainCenteredApp.Services
{
    public class MediaFilesService : IMediaFilesService
    {
        private const string AssetsDirectory = "assets";
        private readonly string[] _allowedExtensions = {".pdf", ".jpg", ".jpeg", ".png"};

        public bool SaveFile(string subDirectoryName, IFormFile file, Guid productId)
        {
            if (file is null ||
                string.IsNullOrWhiteSpace(subDirectoryName) ||
                file.Length > 5_000_000 ||
                !_allowedExtensions.Contains(Path.GetExtension(file.FileName)))
                return false;

            byte[] fileBytes = null; 

            using (var memoryStream = new MemoryStream())
            {
                file.CopyTo(memoryStream);
                fileBytes = memoryStream.ToArray();
            }

            var completePath = Path.Combine(AssetsDirectory, subDirectoryName, productId.ToString(), file.FileName);

            if (!Directory.Exists(Path.Combine(AssetsDirectory, subDirectoryName, productId.ToString())))
                Directory.CreateDirectory(Path.Combine(AssetsDirectory, subDirectoryName, productId.ToString()));

            File.WriteAllBytes(completePath, fileBytes);

            return true;
        }
    }


}
