﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using DomainCenteredApp.Context;
using DomainCenteredApp.Context.Entities;
using DomainCenteredApp.Domain;
using DomainCenteredApp.Domain.Enum;
using DomainCenteredApp.Interfaces.Repositories;
using DomainCenteredApp.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace DomainCenteredApp.DAL.Tests.Repositories
{
    public class ProductRepositoryTests
    {
        private readonly IProductRepository _repo;
        private readonly ProductDbContext _ctx;

        public ProductRepositoryTests(ProductDbContext ctx, IProductRepository repo)
        {
            _repo = repo;
            _ctx = ctx;
        }

        [Fact]
        public void Add_InputIsValid_ReturnsTrue()
        {
            bool result;

            using (var transaction = _ctx.Database.BeginTransaction())
            {
                var product = new ProductDomain
                {
                    Id = Guid.NewGuid(),
                    Category = CategoryEnum.Clothes,
                    Colors = new List<ColorEnum>
                    {
                        ColorEnum.Black,
                        ColorEnum.Pink
                    },
                    Name = "Sexy pijama",
                    Image = "",
                    IsFragile = false,
                    Pdf = "",
                    InsertionDate = DateTime.Now,
                    Description = "Super hot",
                    Weight = 20
                };

                result = _repo.Add(product);
            }

            Assert.True(result);
        }
    }
}
