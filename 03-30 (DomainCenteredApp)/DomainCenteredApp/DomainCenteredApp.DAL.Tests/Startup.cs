﻿using System;
using System.Collections.Generic;
using System.Text;
using DomainCenteredApp.Context;
using DomainCenteredApp.Interfaces.Repositories;
using DomainCenteredApp.Mapper;
using DomainCenteredApp.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DomainCenteredApp.DAL.Tests
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddDbContext<ProductDbContext>(opt =>
                opt.UseSqlServer("Server=LAPTOP-809EEFU0;Database=ProductDb;Trusted_connection=true"));
            services.AddAutoMapper(typeof(MappingProfiles));
        }
    }
}
