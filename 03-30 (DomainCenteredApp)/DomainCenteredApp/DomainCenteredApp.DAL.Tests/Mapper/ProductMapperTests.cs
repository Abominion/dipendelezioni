using System;
using DomainCenteredApp.Context.Entities;
using DomainCenteredApp.Domain;
using DomainCenteredApp.Mapper;
using Xunit;

namespace DomainCenteredApp.DAL.Tests.Mapper
{
    public class ProductMapperTests
    {
        [Fact]
        public void FromEntityToDomain_InputIsValid_ReturnsProductDomain()
        {
            var productEntity = new Product
            {
                Id = Guid.NewGuid(),
                Category = 99,
                Colors = "156,2,3",
                Image = "",
                Pdf = "",
                Name = "HDMI cable",
                InsertionDate = DateTime.Now,
                Description = "Cool HDMI cable",
                IsFragile = false,
                Weight = 0.5
            };

            var productDomain = CustomProductMapper.FromEntityToDomain(productEntity);

            Assert.IsType<ProductDomain>(productDomain);
        }
    }
}
