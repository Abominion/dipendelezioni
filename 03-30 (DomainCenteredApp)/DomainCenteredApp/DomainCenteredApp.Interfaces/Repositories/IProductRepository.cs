﻿using System;
using System.Collections.Generic;
using System.Text;
using DomainCenteredApp.Domain;

namespace DomainCenteredApp.Interfaces.Repositories
{
    public interface IProductRepository
    {
        List<ProductDomain> GetAll();
        bool Add(ProductDomain product);
    }
}
