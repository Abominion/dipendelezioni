﻿using System;
using System.Collections.Generic;
using System.Text;
using DomainCenteredApp.Domain;

namespace DomainCenteredApp.Interfaces.Services
{
    public interface IProductService
    {
        List<ProductDomain> GetAllProducts();
        bool Add(ProductDomain product);
        Guid AddWithGuid(ProductDomain product);
    }
}
