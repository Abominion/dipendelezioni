﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace DomainCenteredApp.Interfaces.Services
{
    public interface IMediaFilesService
    {
        bool SaveFile(string subDirectoryName, IFormFile file, Guid productId);
    }
}
