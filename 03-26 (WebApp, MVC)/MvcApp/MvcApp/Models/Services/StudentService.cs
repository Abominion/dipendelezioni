﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcApp.Models.Services
{
    public class StudentService
    {
        public static List<StudentViewModel> Students { get; set; } = new List<StudentViewModel>
        {
            new StudentViewModel(){ Id = 1, FullName = "Andrea Pollini", Birth = new DateTime(1997,11,20)},
            new StudentViewModel(){ Id = 2, FullName = "Astrid Coltre", Birth = new DateTime(1995,3,10)},
            new StudentViewModel(){ Id = 3, FullName = "Massimiliano De Crignis", Birth = new DateTime(1991,9,14)},
            new StudentViewModel(){ Id = 4, FullName = "Simone Camilletti", Birth = new DateTime(1991,6,15)},
            new StudentViewModel(){ Id = 5, FullName = "Federico Lecca", Birth = new DateTime(1989,4,26)},
            new StudentViewModel(){ Id = 6, FullName = "Davide Loresti", Birth = new DateTime(2000,6,2)},
            new StudentViewModel(){ Id = 7, FullName = "Riccardo Tasca", Birth = new DateTime(1995,10,12)}
        };

        public void Create(StudentViewModel student)
        {
            Students.Add(student);
        }

        public void Edit(StudentViewModel student)
        {
            var toUpdate = Students
                .FirstOrDefault(s => s.Id == student.Id);

            toUpdate.FullName = student.FullName;
            toUpdate.Birth = student.Birth;
        }
    }
}
