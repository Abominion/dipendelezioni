﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MvcApp.Models
{
    public class StudentViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "FullName can't be null")]
        [MinLength(3)]
        [MaxLength(50)]
        public string FullName { get; set; }
        public DateTime Birth { get; set; }
    }
}
