﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Routing;
using MvcApp.Models;
using MvcApp.Models.Services;

namespace MvcApp.Controllers
{
    public class StudentController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View(StudentService.Students);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(StudentViewModel student)
        {
            StudentService.Students.Add(student);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var studentToRemove = StudentService
                .Students
                .FirstOrDefault(student => student.Id == id);

            StudentService.Students.Remove(studentToRemove);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var student = StudentService
                .Students
                .FirstOrDefault(s => s.Id == id);

            return View(student);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var student = StudentService
                .Students
                .FirstOrDefault(s => s.Id == id);

            return View(student);
        }

        [HttpPost]
        public IActionResult Edit(StudentViewModel student)
        {
            if (!ModelState.IsValid) 
                return View(student);

            var service = new StudentService();
            service.Edit(student);

            return RedirectToAction("Details", new {id = student.Id});
        }
    }
}
