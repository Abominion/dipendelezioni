﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkApp.Repositories.Interfaces
{
    public interface IGetOnlyRepository<T>
    {
        T GetById(int id);
        List<T> GetAll();
    }
}
