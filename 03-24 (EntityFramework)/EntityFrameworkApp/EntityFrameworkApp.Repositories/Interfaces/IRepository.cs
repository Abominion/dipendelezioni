﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace EntityFrameworkApp.Repositories.Interfaces
{
    public interface IRepository<T> : IGetOnlyRepository<T>
    {
        void Add(T objectToAdd);
        void Update(T updatedObject);
        void Delete(int id);
    }
}
