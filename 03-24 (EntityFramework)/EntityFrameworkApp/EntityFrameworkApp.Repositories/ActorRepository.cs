﻿using EntityFrameworkApp.Context2;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityFrameworkApp.Context2.Entities;
using EntityFrameworkApp.Repositories.Interfaces;

namespace EntityFrameworkApp.Repositories
{
    public class ActorRepository : IRepository<Actor>
    {
        private readonly CinemaContext _ctx;

        public ActorRepository()
        {
            _ctx = new CinemaContext();
        }

        public Actor GetById(int id)
        {
            var actor = _ctx
                .Actors
                .FirstOrDefault(actor => actor.Id == id);
            return actor;
        }

        public List<Actor> GetAll()
        {
            return _ctx.Actors.ToList();
        }

        public void Add(Actor objectToAdd)
        {
            _ctx.Actors.Add(objectToAdd);
            _ctx.SaveChanges();
        }

        public void Update(Actor updatedObject)
        {
            var actorToUpdate = _ctx
                .Actors
                .FirstOrDefault(actor => actor.Id == updatedObject.Id);

            actorToUpdate.FirstName = updatedObject.FirstName;
            actorToUpdate.LastName = updatedObject.LastName;
            actorToUpdate.DateOfBirth = updatedObject.DateOfBirth;

            _ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            var toDelete = _ctx.Actors.FirstOrDefault(actor => actor.Id == id);
            _ctx.Actors.Remove(toDelete);
            _ctx.SaveChanges();
        }
    }
}
