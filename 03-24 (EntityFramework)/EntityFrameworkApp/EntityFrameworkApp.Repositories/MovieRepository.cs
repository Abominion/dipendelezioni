﻿using System;
using System.Collections.Generic;
using System.Text;
using EntityFrameworkApp.Context2;
using EntityFrameworkApp.Context2.Entities;
using EntityFrameworkApp.Repositories.Interfaces;

namespace EntityFrameworkApp.Repositories
{
    public class MovieRepository : IRepository<Movie>
    {
        private readonly CinemaContext _ctx;

        public MovieRepository()
        {
            _ctx = new CinemaContext();
        }

        public Movie GetById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Movie> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Add(Movie objectToAdd)
        {
            throw new NotImplementedException();
        }

        public void Update(Movie updatedObject)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
