﻿using System;
using System.Collections.Generic;
using System.Text;
using EntityFrameworkApp.Context2.Entities;
using Microsoft.EntityFrameworkCore;

namespace EntityFrameworkApp.Context2
{
    public class CinemaContext : DbContext
    {
        public CinemaContext()
        {
        }

        public CinemaContext(DbContextOptions<CinemaContext> options) : base(options)
        {
        }

        public virtual DbSet<Actor> Actors { get; set; }
        public virtual DbSet<Movie> Movies { get; set; }
        public virtual DbSet<Producer> Producers { get; set; }
        public virtual DbSet<ActorMovie> ActorMovies { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=LAPTOP-809EEFU0;Initial Catalog=CinemaDatabase;Integrated security=true");
            }
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<ActorMovie>()
                .ToTable("ActorMovie")
                .HasKey(am => new {am.ActorId, am.MovieId});

            modelBuilder
                .Entity<Actor>()
                .ToTable("Actor");

            modelBuilder
                .Entity<Movie>()
                .ToTable("Movie");

            modelBuilder
                .Entity<Producer>()
                .ToTable("Producer");
        }
    }
}
