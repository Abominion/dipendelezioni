﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkApp.Context2.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public short ReleaseYear { get; set; }

        public virtual ICollection<ActorMovie> ActorMovies { get; set; }
        public virtual Producer Producer { get; set; }
    }
}
