﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkApp.Context2.Entities
{
    public class Actor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }

        public virtual ICollection<ActorMovie> ActorMovies { get; set; }
    }
}
