﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkApp.Context2.Entities
{
    public class Producer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public short FoundationYear { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }
    }
}
