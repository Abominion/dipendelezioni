﻿using System;
using System.Linq;
using EntityFrameworkApp.Context;
using EntityFrameworkApp.Context.Entities;
using Microsoft.EntityFrameworkCore;

namespace EntityFrameworkApp.Presentation
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new SoftwareDevelopersContext();

            // Read

            var softwareHouse =  context.SoftwareHouse // Deferred execution
                .Include(sh => sh.Videogame)
                .FirstOrDefault(sh => sh.Id == 1);

            var videogames = softwareHouse?.Videogame.ToList();

            Console.WriteLine($"{softwareHouse.Id} - {softwareHouse.CompanyName} {softwareHouse.FoundationYear}");

            // Insert

            var softwareHouseToAdd = new SoftwareHouse
            {
                CompanyName = "Nintendo",
                FoundationYear = 1889
            };

            context.SoftwareHouse.Add(softwareHouseToAdd);
            context.SaveChanges(); // IMPORTANTE!
        }
    }
}
