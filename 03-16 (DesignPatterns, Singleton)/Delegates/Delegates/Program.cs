﻿using System;
using System.Collections.Generic;

namespace Delegates
{
    public delegate int MyDelegate(int addend1, int addend2);

    class Arithmetic
    {
        public delegate int MyDelegate(int number);

        public int Sum(int num1, int num2)
        {
            return num1 + num2;
        }

        public int QuadraticPower(int number)
        {
            return number * number;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Arithmetic arithmetic = new Arithmetic();
            MyDelegate myDelegate = new MyDelegate(arithmetic.Sum);
            var result = myDelegate(2, 2);
            //myDelegate += arithmetic.Sum; // NO! Meglio far puntare ai delegate un solo metodo

            Arithmetic.MyDelegate myDelegate2 = new Arithmetic.MyDelegate(arithmetic.QuadraticPower);
            Arithmetic.MyDelegate myDelegate3 = arithmetic.QuadraticPower;

            Console.WriteLine(myDelegate3(6));

            foreach (var @delegate in myDelegate3.GetInvocationList())
            {
                Console.WriteLine(@delegate.Method.Name);
            }

            myDelegate3 -= arithmetic.QuadraticPower; // Operator overloading che richiama il metodo Remove() di System.Delegate

            //foreach (var @delegate in myDelegate3.GetInvocationList())
            //{
            //    Console.WriteLine(@delegate.Method.Name);
            //}

            /* DELEGATI DI SISTEMA */

            // Esistono tre delegati di base: Action, Func, Predicate

            // Action (accetta metodi void. Prende in ingresso un numero
            // variabile di parametri del tipo che vuoi)

            void Hello(string name)
            {
                Console.WriteLine($"Hello {name}!");
            }

            Action<string> actionHello = Hello;

            actionHello("Cammello");

            // Func (ha un tipo di ritorno.
            // Puoi aggiungere tutti i parametri di tutti i tipi che vuoi)

            bool IsEven(int number)
            {
                return number % 2 == 0;
            }

            Func<int, bool> funcIsEven = new Func<int, bool>(IsEven);

            string ConcatStrings(string str1, string str2)
            {
                return str1 + str2;
            }

            Func<string, string, string> funcConcatStrings = new Func<string, string, string>(ConcatStrings);

            // Predicate (ritorna un bool e prende in ingresso un parametro)

            bool IsBetterThanCSharp(string language)
            {
                return false;
            }

            Predicate<string> predicate = IsBetterThanCSharp;
        }

        delegate T GenericDel<T>(T parameter);
    }
}
