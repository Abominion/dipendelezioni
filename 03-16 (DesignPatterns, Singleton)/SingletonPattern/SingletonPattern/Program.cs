﻿using System;

namespace SingletonPattern // DP creazionale. 
{
    class Singleton
    {
        private static Singleton _singleton;
        public string Name { get; set; }

        private Singleton()
        {

        }

        public static Singleton GetInstance()
        {
            if(_singleton == null)
                _singleton = new Singleton();

            return _singleton;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var singleton1 = Singleton.GetInstance();

            singleton1.Name = "Ascaranzegi";
            Console.WriteLine(singleton1.Name);

            var singleton2 = Singleton.GetInstance();

            singleton2.Name = "Ugo Sugo";
            Console.WriteLine(singleton1.Name);
            Console.WriteLine(singleton2.Name);
        }
    }
}
