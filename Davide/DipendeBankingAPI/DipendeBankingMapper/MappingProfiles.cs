﻿using AutoMapper;
using DipendeBankingContext.Entities;
using DipendeBankingDomains;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DipendeBankingMapper
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Customer, CustomerDomain>().ReverseMap();
            CreateMap<Account, AccountDomain>().ReverseMap();
        }
    }
}
