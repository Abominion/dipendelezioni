﻿using DipendeBankingDomains;
using DipendeBankingServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DipendeBankingAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccountController : Controller
    {
        private readonly AccountSerivce service;

        public AccountController(AccountSerivce _service)
        {
            service = _service;
        }

        [HttpGet]
        public ActionResult<IEnumerable<AccountDomain>> Get()
        {
            try
            {
                return Ok(service.Get());
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<AccountDomain> Get(int id)
        {
            try
            {
                return Ok(service.Get(id));
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                service.Delete(id);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut]
        public ActionResult Put(AccountDomain objDomainToUpdate)
        {
            try
            {
                service.Put(objDomainToUpdate);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost]
        public ActionResult Post(AccountDomain objDomainToUpdate)
        {
            try
            {
                service.Post(objDomainToUpdate);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
