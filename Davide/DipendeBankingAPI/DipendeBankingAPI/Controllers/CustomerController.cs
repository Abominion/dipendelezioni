﻿using DipendeBankingDomains;
using DipendeBankingServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DipendeBankingAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : Controller
    {
        private readonly CustomerService service;

        public CustomerController(CustomerService _service)
        {
            service = _service;
        }


        [HttpGet]
        public ActionResult<IEnumerable<CustomerDomain>> Get()
        {
            try
            {
                return Ok(service.Get());
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<CustomerDomain> Get(int id)
        {
            try
            {
                return Ok(service.Get(id));
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                service.Delete(id);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPut]
        public ActionResult Put(CustomerDomain objDomainToUpdate)
        {
            try
            {
                service.Put(objDomainToUpdate);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost]
        public ActionResult Post(CustomerDomain objDomainToUpdate)
        {
            try
            {
                service.Post(objDomainToUpdate);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
