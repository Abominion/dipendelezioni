﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipendeBankingDomains
{
    public class AccountDomain
    {
        public int Id { get; set; }
        public double Deposit { get; set; }
        public CustomerDomain Customer_FK { get; set; }
    }
}
