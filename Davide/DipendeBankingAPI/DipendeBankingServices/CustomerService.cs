﻿using DipendeBankingDomains;
using DipendeBankingInterfaces;
using System;
using System.Collections.Generic;

namespace DipendeBankingServices
{
    public class CustomerService
    {
        private readonly ICustomerRepository repos;

        public CustomerService(ICustomerRepository _repos)
        {
            repos = _repos;
        }


        public void Put(CustomerDomain objDomainToUpdate)
        {
            repos.Update(objDomainToUpdate);
        }

        public void Delete(int id)
        {
            repos.Delete(id);
        }

        public void Post(CustomerDomain objDomainToAdd)
        {
            repos.Add(objDomainToAdd);
        }

        public List<CustomerDomain> Get()
        {
            return repos.GetAll();
        }

        public CustomerDomain Get(int id)
        {
            return repos.GetById(id);
        }
    }
}
