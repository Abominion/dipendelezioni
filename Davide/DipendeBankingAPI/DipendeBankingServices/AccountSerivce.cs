﻿using DipendeBankingDomains;
using DipendeBankingInterfaces;
using System;
using System.Collections.Generic;

namespace DipendeBankingServices
{
    public class AccountSerivce
    {
        private readonly IAccountRepository repos;

        public AccountSerivce(IAccountRepository _repos)
        {
            repos = _repos;
        }


        public void Put(AccountDomain objDomainToUpdate)
        {
            repos.Update(objDomainToUpdate);
        }

        public void Delete(int id)
        {
            repos.Delete(id);
        }

        public void Post(AccountDomain objDomainToAdd)
        {
            repos.Add(objDomainToAdd);
        }

        public List<AccountDomain> Get()
        {
            return repos.GetAll();
        }

        public AccountDomain Get(int id)
        {
            return repos.GetById(id);
        }
    }
}
