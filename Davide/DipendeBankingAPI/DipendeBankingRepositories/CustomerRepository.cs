﻿using AutoMapper;
using DipendeBankingContext.DBContext;
using DipendeBankingContext.Entities;
using DipendeBankingDomains;
using DipendeBankingInterfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipendeBankingRepositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IMapper mapper;
        private readonly DipendeBankingDBContext ctx;

        public CustomerRepository(IMapper _mapper, DipendeBankingDBContext _ctx)
        {
            mapper = _mapper;
            ctx = _ctx;
        }


        public void Add(CustomerDomain objDomainToAdd)
        {
            var customerToAdd = mapper.Map<Customer>(objDomainToAdd);
            ctx.Customers.Add(customerToAdd);
            ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            var customerToDelete = ctx.Customers.SingleOrDefault(customer => customer.Id == id);
            ctx.Customers.Remove(customerToDelete);
            ctx.SaveChanges();
        }

        public List<CustomerDomain> GetAll()
        {
            var customerEntities = ctx.Customers;
            var customerDomainList = mapper.ProjectTo<CustomerDomain>(customerEntities).ToList();

            return customerDomainList;
        }

        public CustomerDomain GetById(int id)
        {

            var customerToGet = ctx.Customers.Include(c => c.accounts).SingleOrDefault(customer => customer.Id == id);
            var customerToGetToDomain = mapper.Map<CustomerDomain>(customerToGet);

            return customerToGetToDomain;
        }

        public void Update(CustomerDomain objDomainToUpdate)
        {
            var customerEntitiesToUpdate = mapper.Map<Customer>(objDomainToUpdate);
            ctx.Customers.Update(customerEntitiesToUpdate);
            ctx.SaveChanges();
        }
    }
}
