﻿using AutoMapper;
using DipendeBankingContext.DBContext;
using DipendeBankingContext.Entities;
using DipendeBankingDomains;
using DipendeBankingInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipendeBankingRepositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly IMapper mapper;
        private readonly DipendeBankingDBContext ctx;

        public AccountRepository(IMapper _mapper, DipendeBankingDBContext _ctx)
        {
            mapper = _mapper;
            ctx = _ctx;
        }


        public void Add(AccountDomain objDomainToAdd)
        {
            var accountToAdd = mapper.Map<Account>(objDomainToAdd);
            ctx.Accounts.Add(accountToAdd);
            ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            var accountToDelete = ctx.Accounts.SingleOrDefault(account => account.Id == id);
            ctx.Accounts.Remove(accountToDelete);
            ctx.SaveChanges();
        }

        public List<AccountDomain> GetAll()
        {
            var accountEntities = ctx.Accounts;
            var accountDomainList = mapper.ProjectTo<AccountDomain>(accountEntities).ToList();

            return accountDomainList;
        }

        public AccountDomain GetById(int id)
        {
            var accountToGet = ctx.Accounts.SingleOrDefault(account => account.Id == id);
            var accountToGetToDomain = mapper.Map<AccountDomain>(accountToGet);

            return accountToGetToDomain;
        }

        public void Update(AccountDomain objDomainToUpdate)
        {
            var accountEntitiesToUpdate = mapper.Map<Account>(objDomainToUpdate);
            ctx.Accounts.Update(accountEntitiesToUpdate);
            ctx.SaveChanges();
        }
    }
}
