﻿using DipendeBankingContext.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace DipendeBankingContext.DBContext
{
    public class DipendeBankingDBContext : DbContext
    {
        public DipendeBankingDBContext()
        {

        }

        public DipendeBankingDBContext(DbContextOptions<DipendeBankingDBContext> options) : base(options)
        {

        }

        public virtual DbSet<Customer> Customers {get; set;}
        public virtual DbSet<Account> Accounts {get; set;}
    }
}
