﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipendeBankingContext.Entities
{
    public class Account
    {
        public int Id { get; set; }
        public double Deposit { get; set; }
        public Customer Customer_FK { get; set; }
    }
}
