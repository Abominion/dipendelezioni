﻿using System;
using DipendeBankingDomains;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipendeBankingInterfaces
{
    public interface ICustomerRepository : IRepository<CustomerDomain>
    {
    }
}
