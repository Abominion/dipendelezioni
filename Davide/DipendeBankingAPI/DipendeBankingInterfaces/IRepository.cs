﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipendeBankingInterfaces
{
    public interface IRepository<T>
    {
        public void Add(T objDomainToAdd);
        public void Update(T objDomainToUpdate);
        public void Delete(int id);
        public T GetById(int id);
        public List<T> GetAll();
    }
}
