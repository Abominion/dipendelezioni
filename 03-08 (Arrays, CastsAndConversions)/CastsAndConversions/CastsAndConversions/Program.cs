﻿using System;
using System.Transactions;

namespace CastsAndConversions
{
    class Program
    {
        static void Main(string[] args)
        {
            /* CASTS AND CONVERSIONS */

            // In entrambi i casi si tratta di cambiare il tipo di un oggetto in un altro

            /* Una differenza fondamentale:
             - Un cast cambia il tipo. E' reversibile
             - Una conversion cambia il valore, se necessario INSIEME al tipo */

            /* --- Cast --- */

            // Si dividono in impliciti ed espliciti

            // Implicit casts - il cast è implicitamente effettuato dal compiler. Non perdi informazioni

            // (Value types)

            int tinyInteger = 10;
            long bigInteger = tinyInteger; // Contenitore più grande, no perdita di informazioni

            float tinyReal = 10.5f;
            double bigReal = tinyReal;

            //double bigReal2 = Double.MaxValue;
            //float tinyReal2 = bigReal2;

            // (Reference types)

            NotSupportedException derivedException = new NotSupportedException();
            Exception baseException = derivedException; // NotSupportedException deriva da Exception

            // Explicit casts - la conversione non è effettuata in modo implicito dal compiler, necessita di un cast operator
            // Ciò significa che la conversione può fallire o può causare la perdita di dati

            // (ValueTypes)

            double precise = Math.Cos(1.234 * Math.PI) / Math.Sin(1.2345);
            float coarse = (float) precise;

            float doubleMaxValue = (float) Double.MaxValue;

            long longMaxValue = long.MaxValue - 1L;
            int longToInt = (int) longMaxValue;

            //string text = "123";
            //double value = (double) text; // Naturalmente questo non compila!

            // (Reference types)

            object @object = 1;

            int objectToInt = (int) @object;

            // @object = "Hello";
            // objectToInt = (int) @object; // Questo compila, ma fallisce a runtime

            /* --- Conversions --- */

            // Servono per trasformazioni più esplicite, anche tra tipi di dato tra loro incompatibili

            double fromString1 = double.Parse("4.8"); // Compila e funziona, ma è soggetto a possibili eccezioni
            // fromString1 = double.Parse("Hello"); // Lancia eccezione

            //bool isCastAllowed = double.TryParse("Hello", out double doubleValue);
            
            bool isCastAllowed = double.TryParse("2.5", out double doubleValue);

            Console.WriteLine(isCastAllowed ? doubleValue.ToString() : "Nope"); // Anche il ToString  è una conversion

            Console.WriteLine(Convert.ToInt32(true));

            Console.WriteLine(Convert.ToInt32("25"));

            // Occhio alle approssimazioni:

            double real = 1.3;

            int castInteger = (int) real;

            int convertedInteger = Convert.ToInt32(real); // Effettua un round (approssimazione per eccesso o per difetto) 

            Console.WriteLine($"castInteger: {castInteger}");
            Console.WriteLine($"convertedInteger: {convertedInteger}");

            // E con i nullable types?

            float? maybe = null; // è come dire Nullable<float> maybe = 10;
            //float sure1 = (float) maybe;
            //float sure2 = maybe.Value;
            float sure3 = maybe.GetValueOrDefault();
        }
    }
}
