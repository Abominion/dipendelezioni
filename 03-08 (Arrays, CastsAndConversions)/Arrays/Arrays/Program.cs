﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            /* ARRAY */

            // Gli array sono OGGETTI (tipi riferimento). La classe base per tutti gli array è System.Array

            int[] numeriInteri = new int[4] {1, 2, 10, 25};

            // Dichiarazioni 

            int[] numeri1 = new int[] {1,2,10,15};
            int[] numeri2 = {1,2,10,15};
            
            foreach (var numero in numeriInteri)
            {
                Console.WriteLine(numero);
            }

            Console.WriteLine(numeriInteri is int[]);
            Console.WriteLine(numeriInteri is Array);

            int index = 0;

            while (index < numeriInteri.Length)
            {
                Console.Write($"{numeriInteri[index++]} ");
            }

            Console.WriteLine();
            index = 0;

            do
            {
                Console.Write($"{numeriInteri[index++]} ");
            } while (index < numeriInteri.Length);

            Console.WriteLine();

            for (int i = 0; i < numeriInteri.Length; i++)
            {
                Console.Write($"{numeriInteri[i]} ");
            }

            // MATRICI 

            /* Matrici regolari */

            int[,] matriceBidimensionale = new int[3,4];
            matriceBidimensionale[0, 0] = 10;
            matriceBidimensionale[0, 1] = 20;
            matriceBidimensionale[0, 2] = 30;

            Console.WriteLine();

            int[,] matriceBidimensionale2 =
            {
                {1, 2, 3, 8},
                {4, 4, 0, 5}
            };

            Console.WriteLine($"Numero totale elementi: {matriceBidimensionale.Length}");
            Console.WriteLine($"Numero rows: {matriceBidimensionale.Rank}");
            Console.WriteLine($"Dimensione matrice all'indice 0: {matriceBidimensionale.GetLength(0)}");
            Console.WriteLine($"Dimensione matrice all'indice 1: {matriceBidimensionale.GetLength(1)}");

            for (int row = 0; row < matriceBidimensionale.GetLength(0); ++row)
            {
                Console.Write("[");

                for (int column = 0; column < matriceBidimensionale.GetLength(1); column++)
                {
                    Console.Write($"{matriceBidimensionale[row,column], -5}");
                }

                Console.WriteLine("]");
            }

            /* Matrici irregolari (jagged arrays) */

            int[][] irregular = new int[3][];

            irregular[0] = new int[] {4,2,6};
            irregular[1] = new int[] {0};
            irregular[2] = new int[] {4,2,6,324,5466,867,87,889,89,8,7,4};

            for (int row = 0; row < irregular.Length; row++)
            {
                for (int column = 0; column < irregular[row].Length; column++)
                {
                    Console.Write(irregular[row][column] + " ");
                }

                Console.WriteLine();
            }

            /* Metodi utili sugli array */

            int[] unorderedArray = { 10,5,20,0,2,1 };

            Array.Sort(unorderedArray);

            Console.WriteLine(string.Join(',', unorderedArray));

            Array.Clear(unorderedArray, 0, 5);

            Console.WriteLine(string.Join(',', unorderedArray));

            PrintArray(unorderedArray);

            Console.WriteLine(Sum("Ciao", 2, 3, 4, 5));
        }

        static void PrintArray(int[] arr)
        {
            foreach (var element in arr)
            {
                Console.Write($"{element} ");
            }
        }

        static int Sum(string name, params int[] addends)
        {
            int sum = 0;

            foreach (var addend in addends)
            {
                sum += addend;
            }

            return sum;
        }
    }
}
