﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using JwtAuthentication.Configuration;
using JwtAuthentication.Interfaces;
using JwtAuthentication.Models.Dtos.Requests;
using JwtAuthentication.Models.Dtos.Responses;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace JwtAuthentication.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthManagementController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IAuthenticationService _authenticationService;


        public AuthManagementController(UserManager<IdentityUser> userManager, IAuthenticationService authenticationService)
        {
            _userManager = userManager;
            _authenticationService = authenticationService;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] UserRegistrationRequestData user)
        {
            #region ModelState Is Invalid

            if (!ModelState.IsValid)
                return BadRequest(new RegistrationResponse()
                {
                    Result = false,
                    Errors = new List<string>() { "Invalid payload" }
                });


            #endregion

            // check if a user with the same email exist
            var existingUser = await _userManager.FindByEmailAsync(user.Email);

            #region User With Same Email Already Exists

            if (existingUser != null)
            {
                return BadRequest(new RegistrationResponse()
                {
                    Result = false,
                    Errors = new List<string>() { "Email already exist" }
                });
            }

            #endregion

            var newUser = new IdentityUser() { Email = user.Email, UserName = user.Email };
            var isCreated = await _userManager.CreateAsync(newUser, user.Password);

            #region User Succesfully Created

            if (isCreated.Succeeded)
            {
                var jwtToken = _authenticationService.GenerateJwtToken(newUser);

                return Ok(new RegistrationResponse()
                {
                    Result = true,
                    Token = jwtToken
                });
            }

            #endregion

            #region Error Creating User

            return new JsonResult(new RegistrationResponse()
            {
                Result = false,
                Errors = isCreated.Errors.Select(x => x.Description).ToList()
            })
            { StatusCode = 500 };

            #endregion
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] UserLoginRequestData user)
        {
            #region ModelState Is Invalid

            if (!ModelState.IsValid)
                return BadRequest(new RegistrationResponse()
                {
                    Result = false,
                    Errors = new List<string>(){
                        "Invalid payload"
                    }
                });

            #endregion

            // check if the user with the same email exist
            var existingUser = await _userManager.FindByEmailAsync(user.Email);

            #region User With Provided Email Doesn't Exist

            if (existingUser == null)
            {
                // We dont want to give to much information on why the request has failed for security reasons
                return BadRequest(new RegistrationResponse()
                {
                    Result = false,
                    Errors = new List<string>(){
                            "Invalid authentication request"
                        }
                });
            }

            #endregion

            // Now we need to check if the user has inputed the right password
            var isCorrect = await _userManager.CheckPasswordAsync(existingUser, user.Password);

            #region Provided Password Is Incorrect

            if (!isCorrect)
                // We dont want to give to much information on why the request has failed for security reasons
                return BadRequest(new RegistrationResponse()
                {
                    Result = false,
                    Errors = new List<string>(){
                            "Invalid authentication request"
                        }
                });

            #endregion

            #region Provided Password Is Correct

            var jwtToken = _authenticationService.GenerateJwtToken(existingUser);

            return Ok(new RegistrationResponse()
            {
                Result = true,
                Token = jwtToken
            });

            #endregion
        }
    }
}
