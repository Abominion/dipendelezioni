﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace JwtAuthentication.Interfaces
{
    public interface IAuthenticationService
    {
        string GenerateJwtToken(IdentityUser user);
    }
}
