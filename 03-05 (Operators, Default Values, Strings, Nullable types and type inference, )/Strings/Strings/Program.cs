﻿using System;
using System.Text;

namespace Strings
{
    class Program
    {
        static void Main(string[] args)
        {
            string city = "Rome";

            Console.WriteLine(city[0]);

            Console.WriteLine(city is Array);

            // string city2 = "Rome"; // Non viene allocata una nuova cella di memoria in heap, viene riutilizzata quella di city

            string city2 = city; 

            city2 = city2 + " in Italy";

            /* Methods and properties */

            string myName = null;

            myName = "";
            myName = string.Empty;
            myName = "Lorenzo";

            Console.WriteLine(myName.Length); // Lunghezza della stringa
            Console.WriteLine(myName.IndexOf('O')); // Restituisce l'indice della prima occorrenza di un carattere
            Console.WriteLine(myName.IndexOf('o', 2));
            Console.WriteLine(myName[myName.IndexOf('o')]);
            Console.WriteLine(myName.LastIndexOf('o'));
            Console.WriteLine(myName.ToUpper());
            Console.WriteLine(myName.ToLower());

            string substring = myName.Substring(1, 2);
            Console.WriteLine(substring);

            Console.WriteLine(myName.Insert(1, "blabla"));

            string greetings = "                           hello                              ";

            Console.WriteLine(myName.Trim('o'));
            Console.WriteLine(myName.Trim('o', 'L'));

            Console.WriteLine(string.Concat(greetings, " ", myName));

            //DateTime start = DateTime.Now;
            //string accumulator = string.Empty;

            //for (int i = 0; i < 200000; i++)
            //{
            //    accumulator += "!";
            //}

            //DateTime end = DateTime.Now;
            //Console.WriteLine("Time taken (concat): " + (end - start));

            //DateTime start2 = DateTime.Now;
            //StringBuilder builder = new StringBuilder(string.Empty);

            //for (int i = 0; i < 200000; i++)
            //{
            //    builder.Append("!");
            //}

            //DateTime end2 = DateTime.Now;
            //Console.WriteLine("Time taken (StringBuilder): " + (end2 - start2));

            //bool equalityCheck = greetings == myName;
            //bool equalityCheck = greetings.Equals(myName);
            bool equalityCheck = string.Equals(greetings, myName);

            string upperCaseDog = "SHONNY";
            string lowerCaseDog = "shonny";

            Console.WriteLine("SHONNY == shonny: " + upperCaseDog.Equals(lowerCaseDog));
            Console.WriteLine("shonny == shonny: " + upperCaseDog.ToLower().Equals(lowerCaseDog));
            Console.WriteLine(string.IsNullOrWhiteSpace(" "));
            
            Console.WriteLine(upperCaseDog.Replace('N', 'P'));

            Console.WriteLine(string.Format("Un etto di {1} fanno {0} {2}", 2.33, "prosciutto", "€"));

            string interpolatedString = $"My name is {upperCaseDog}";
            string notInterpolatedString = "My name is {upperCaseDog}";

            Console.WriteLine(interpolatedString);
            Console.WriteLine(notInterpolatedString);

            string noVerbatim = "He said,\n \"This is the last \u0063hance\x0021\"";
            Console.WriteLine(noVerbatim);

            string withVerbatim = $@"{lowerCaseDog} said,\n ""This is the last \u0063hance\x0021""";
            Console.WriteLine(withVerbatim);
        }
    }
}
