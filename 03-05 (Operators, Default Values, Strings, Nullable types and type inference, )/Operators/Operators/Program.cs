﻿using System;
using System.Text;

namespace Operators
{
    class Program
    {
        static void Main(string[] args)
        {
            /* OPERATORS */

            // Gli operatori possono essere:
            // - aritmetici, logici, di comparazione(o di raffronto), bitwise, assegnazione
            // - unari, binari, ternari

            int left = 4;
            int right = 4;

            /* Arithmetic operators */

            int sum = left + right;
            Console.WriteLine($"left + right = {sum}");
            int difference = left - right;
            Console.WriteLine($"left - right = {difference}");
            int product = left * right;
            Console.WriteLine($"left * right = {product}");
            int quotient = left / right;
            Console.WriteLine($"left / right = {quotient}");

            int increment1 = sum++;
            // sum ora è uguale a 9
            Console.WriteLine($"sum++ = {increment1}");
            int increment2 = ++sum;
            // sum ora è uguale a 10
            Console.WriteLine($"++sum = {increment2}");

            increment1 = sum--;
            // sum ora è uguale a 9
            Console.WriteLine($"sum-- = {increment1}");
            increment2 = --sum;
            // sum ora è uguale a 8
            Console.WriteLine($"--sum = {increment2}");

            int modulo = left % right; // restituisce il resto della divisione tra i due operandi
            Console.WriteLine($"left % right = {modulo}");

            /* Relational operators */

            bool equals = 2 == 2;
            Console.WriteLine($"2 == 2: {equals}");

            bool notEquals = 2 != 2;
            Console.WriteLine($"2 != 2: {notEquals}");

            bool higherThan = 2 > 2;
            Console.WriteLine($"2 > 2: {higherThan}");

            bool lessThan = 2 < 2;
            Console.WriteLine($"2 < 2: {lessThan}");

            bool higherOrEqualThan = 2 >= 2;
            Console.WriteLine($"2 >= 2: {higherOrEqualThan}");

            bool lessOrEqualThan = 2 <= 2;
            Console.WriteLine($"2 <= 2: {lessOrEqualThan}");

            /* --- Logical operators --- */

            bool logicalAnd = 2 == 2 && 1 == 1; // L'and logico si avvale dello short circuiting, ovver, se l'espressione di sinistra
                                                // è false non valuta neanche quella di destra
            Console.WriteLine($"2 == 2 && 1 == 1: {logicalAnd}");

            bool logicalOr = 2 == 2 || 1 == 1;
            Console.WriteLine($"2 == 2 || 1 == 1: {logicalOr}");

            bool logicalNot = !logicalOr;
            Console.WriteLine($"!logicalOr: {logicalNot}");

            /* --- Bitwise operators --- */

            int binary1 = 0b00000111; // 7
            int binary2 = 0b00001010; // 10

            int bitwiseAnd = binary1 & binary2;

            Console.WriteLine(bitwiseAnd);

            int bitwiseOr = binary1 | binary2;

            Console.WriteLine(bitwiseOr);

            int bitwiseXor = binary1 ^ binary2;

            Console.WriteLine(bitwiseXor);

            int bitwiseNot = ~binary1; // ~ si fa con alt + 126
            Console.WriteLine(bitwiseNot);

            int bitwiseLeftShift = binary1 << 1;
            
            int bitwiseRightShift = binary1 >> 1;

            // Utilizzi pratici dei bitwise

            int number = 10;
            Console.WriteLine(~number + 1); // Inversione del segno

            int toSwap1 = 10;
            int toSwap2 = 20;

            Console.WriteLine($"Old toSwap1: {toSwap1}");
            Console.WriteLine($"Old toSwap2: {toSwap2}");

            toSwap1 = toSwap1 ^ toSwap2;
            toSwap2 = toSwap2 ^ toSwap1;
            toSwap1 = toSwap1 ^ toSwap2;

            Console.WriteLine($"New toSwap1: {toSwap1}");
            Console.WriteLine($"New toSwap2: {toSwap2}");

            int toTwiceTheValue = 10 << 1;
            Console.WriteLine($"10 * 2 = {toTwiceTheValue}");

            /* Assignment operators */

            number = 1;

            number += 2; // equivalente di number = number + 2;
            number -= 2;
            number *= 2;
            number /= 2;
            number %= 2;
            number <<= 2;
            number >>= 2;
            number &= 2;
            number ^= 2;
            number |= 2;

            /* Miscellaneous operators */

            Console.WriteLine($"sizeof(bool): {sizeof(bool)}"); // Funziona solo coi valuetypes

            Console.WriteLine($"typeof(StringBuilder): {typeof(StringBuilder)}");

            int myNumber = 10;
            int ternaryResult = (myNumber > 1) ? myNumber : 0;

            Console.WriteLine($"number is Random: {number is Random}");
            Console.WriteLine($"number is int: {number is int}");

            string myString = null;
            string myString2 = myString ?? "Hello"; // Null coalesce operator

            object obj = new StringBuilder();
            // var stringBuilder = obj as StringBuilder; // Ok
            var stringBuilder = obj as string; // Non posso convertire direttamente uno StringBuilder in string, quindi la variabile resterà null
        }
    }
}
