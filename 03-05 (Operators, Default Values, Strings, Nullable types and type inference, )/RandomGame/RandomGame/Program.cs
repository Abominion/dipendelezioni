﻿using System;

namespace RandomGame
{
    class Program
    {
        static void Main(string[] args)
        {
            int counter = 0;

            Console.Write("Hi, guess a number between 0 and 50: ");
            
            while (counter < 3)
            {
                string chosenNumber = Console.ReadLine();

                int randomNumber = new Random().Next(0, 51);

                int parsedChosenNumber = int.Parse(chosenNumber);

                Console.WriteLine($"Generated number: {parsedChosenNumber} - Chosen number {chosenNumber}");

                if (parsedChosenNumber == randomNumber)
                {
                    Console.WriteLine("Wow, good job!");
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine("Nope, try again.");
                }
            }

        }
    }
}
