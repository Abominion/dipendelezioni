﻿using System;

namespace DefaultValues
{
    class Program
    {
        private static int _intDefault; // Field, viene allocata in heap. Valore di default 0
        private static float _floatDefault; // 0
        private static double _doubleDefault; // 0
        private static decimal _decimalDefault; // 0
        private static bool _boolDefault; // false
        private static DateTime _dateTimeDefault; // 01/01/0001 00:00:00
        private static string _stringDefault; // null


        static void Main(string[] args)
        {
            Console.WriteLine(_intDefault);

            int localInt; // Local variable
            // Console.WriteLine(localInt); // Non puoi usare una variabile locale prima di averla inizializzata
        }
    }
}
