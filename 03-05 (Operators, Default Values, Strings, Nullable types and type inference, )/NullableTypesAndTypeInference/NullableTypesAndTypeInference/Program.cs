﻿using System;

namespace NullableTypesAndTypeInference
{
    class Program
    {
        static void Main(string[] args)
        {
            /* NULLABLE TYPES */

            int number = 1;

            int? nullableNumber = null; // sugar syntax per Nullable<int> nullableNumber = null;

            int? refInt1 = 10; // 10

            int? refInt2 = refInt1; // 10

            ++refInt1; // 11

            Console.WriteLine(refInt1); // 11
            Console.WriteLine(refInt2); // 10

            if (nullableNumber.HasValue) 
            {
                Console.WriteLine("number has a value");
            }
            else
            {
                Console.WriteLine("number doesn't have a value");
            }

            int? result = number + nullableNumber;

            Console.WriteLine(result);

            //int? result2 = number + (int) nullableNumber;
            //int? result3 = number + nullableNumber.Value;

            /* TYPE INFERENCE */

            var myName = "Lorenzo";
            
            var myNumber = 1L; // Per far capire alla CLR che vogliamo che lo tratti come Long, occorre il suffisso (e vale per tutti i tipi numerici non standard)
        }
    }
}
